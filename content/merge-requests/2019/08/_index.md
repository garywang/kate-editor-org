---
title: Merge Requests - August 2019
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2019/08/
---

#### Week 35

- **Kate - [Plugin search fixes](https://invent.kde.org/utilities/kate/-/merge_requests/10)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

#### Week 34

- **Kate - [Avoid copying if it&#x27;s possible](https://invent.kde.org/utilities/kate/-/merge_requests/9)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged at creation day.

- **Kate - [Add .clang-format file.](https://invent.kde.org/utilities/kate/-/merge_requests/7)**<br />
Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer) and merged after 3 days.

- **Kate - [Buildplugin enhancements](https://invent.kde.org/utilities/kate/-/merge_requests/6)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 3 days.

#### Week 33

- **Kate - [Rename kate target to kate-lib to fix Windows compilation error](https://invent.kde.org/utilities/kate/-/merge_requests/5)**<br />
Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer) and merged at creation day.

- **Kate - [Main CMake scripts cleanup](https://invent.kde.org/utilities/kate/-/merge_requests/4)**<br />
Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer) and merged at creation day.

- **Kate - [Further cleanup addons CMake scripts.](https://invent.kde.org/utilities/kate/-/merge_requests/3)**<br />
Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer) and merged at creation day.

- **Kate - [katequickopen: add config option to present current or all project files](https://invent.kde.org/utilities/kate/-/merge_requests/2)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

#### Week 32

- **Kate - [build-plugin: also accept + in message filename detection](https://invent.kde.org/utilities/kate/-/merge_requests/1)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **kate-editor.org - [Add links to kde.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/4)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after one day.

#### Week 31

- **kate-editor.org - [Fix margin in team page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/3)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **kate-editor.org - [Add more information about kate in the homepage](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/1)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

