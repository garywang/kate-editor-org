---
title: Merge Requests - November 2019
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2019/11/
---

#### Week 47

- **Kate - [Read the user visible name of the default color scheme](https://invent.kde.org/utilities/kate/-/merge_requests/46)**<br />
Request authored by [David Redondo](https://invent.kde.org/davidre) and merged after 11 days.

#### Week 46

- **Kate - [Add external tools for formatting XML and JSON content](https://invent.kde.org/utilities/kate/-/merge_requests/45)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

#### Week 45

- **Kate - [Remove redundant and c style casts](https://invent.kde.org/utilities/kate/-/merge_requests/42)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged after 22 days.

