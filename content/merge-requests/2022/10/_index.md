---
title: Merge Requests - October 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/10/
---

#### Week 40

- **Kate - [close the first untitled unmodified document on open](https://invent.kde.org/utilities/kate/-/merge_requests/937)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Port away from KDirOperator::actionCollection](https://invent.kde.org/utilities/kate/-/merge_requests/931)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 3 days.

- **Kate - [Enable semantic highlighting by default](https://invent.kde.org/utilities/kate/-/merge_requests/934)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [filetree: small improvements to mini toolbar](https://invent.kde.org/utilities/kate/-/merge_requests/936)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KSyntaxHighlighting - [Add opendocument &quot;flat XML&quot; file types](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/367)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **Kate - [build-plugin: Update run button after model edit](https://invent.kde.org/utilities/kate/-/merge_requests/935)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Add docs for quickopen](https://invent.kde.org/utilities/kate/-/merge_requests/930)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Unify (remove) plugin content margins](https://invent.kde.org/utilities/kate/-/merge_requests/929)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **KTextEditor - [Remove &quot;Preview:&quot; label in themes tab](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/427)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **kate-editor.org - [Handle generation ourselves, let Scripty do extraction only](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/42)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after one day.

- **Kate - [search: do not run if there are no documents open](https://invent.kde.org/utilities/kate/-/merge_requests/928)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [Add more build-plugin target model index checking](https://invent.kde.org/utilities/kate/-/merge_requests/927)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [LSPPlugin: don&#x27;t show a close button on Diagnostics tab](https://invent.kde.org/utilities/kate/-/merge_requests/926)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **Kate - [project: Add ability to exclude folders](https://invent.kde.org/utilities/kate/-/merge_requests/925)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Emit projectMapChanged when .kateproject is modified](https://invent.kde.org/utilities/kate/-/merge_requests/923)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Add Qt6 windows CI support](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/426)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [Add saving the manually added run commands](https://invent.kde.org/utilities/kate/-/merge_requests/922)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Update our .kateproject, make it actually usable](https://invent.kde.org/utilities/kate/-/merge_requests/924)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Build: Allow run commands](https://invent.kde.org/utilities/kate/-/merge_requests/921)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Port away from soon-to-be-deprecated codecForName](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/425)**<br />
Request authored by [Sune Vuorela](https://invent.kde.org/sune) and merged at creation day.

- **Kate - [[WelcomeView] Handle loading/unloading of the Project plugin](https://invent.kde.org/utilities/kate/-/merge_requests/918)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **Kate - [build: Use QPersistentModelIndex](https://invent.kde.org/utilities/kate/-/merge_requests/920)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [[WelcomeView] Improve locating a file in the file manager](https://invent.kde.org/utilities/kate/-/merge_requests/919)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **KSyntaxHighlighting - [Add folding to elixir code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/365)**<br />
Request authored by [Vitor Trindade](https://invent.kde.org/vitortrin) and merged at creation day.

#### Week 39

- **Kate - [Improve fuzzy matching](https://invent.kde.org/utilities/kate/-/merge_requests/916)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [quickopen: Allow wildcard matching (optionally)](https://invent.kde.org/utilities/kate/-/merge_requests/912)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Fix lru tab switching when there are widgets](https://invent.kde.org/utilities/kate/-/merge_requests/913)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Move away from interal fuzzy algo to KFuzzyMatcher](https://invent.kde.org/utilities/kate/-/merge_requests/915)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [ecma_parser: add space after &#x27;class&#x27; and &#x27;function&#x27; keywords](https://invent.kde.org/utilities/kate/-/merge_requests/914)**<br />
Request authored by [Alain Laporte](https://invent.kde.org/alainl) and merged after one day.

- **KTextEditor - [Restore always current highlight/mode settings on reload](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/424)**<br />
Request authored by [loh.tar](https://invent.kde.org/lohtar) and merged after one day.

- **KTextEditor - [Some tests and small refactor to KateViewInternal::word{Prev,Next}](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/422)**<br />
Request authored by [Daniel Contreras](https://invent.kde.org/inextremares) and merged after 2 days.

- **KSyntaxHighlighting - [Add Qt 6 Windows CI](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/364)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

