---
title: 'GSoC 2012: Vi Input Mode'
author: Erlend Hamberg

date: 2012-06-25T20:45:28+00:00
url: /2012/06/25/gsoc-2012-vi-input-mode/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE

---
[<img class="alignright  wp-image-1815" title="300x400" src="/wp-content/uploads/2012/06/300x400-225x300.jpg" alt="" width="158" height="210" srcset="/wp-content/uploads/2012/06/300x400-225x300.jpg 225w, /wp-content/uploads/2012/06/300x400.jpg 300w" sizes="(max-width: 158px) 100vw, 158px" />][1]Like last summer I am mentoring a student working on Kate&#8217;s Vi mode this summer. This year&#8217;s student is Vegard Øye from Oslo, Norway. I&#8217;ll let him introduce himself:

> My name is Vegard Øye. I am a computer science student at the University of Oslo, Norway. I also have a bachelor&#8217;s degree in electrical engineering from Sør-Trøndelag University College in Trondheim, where I programmed for integrated circuits. It was a lot of fun, so I decided to embark on a grade with an even larger emphasis on programming.
> 
> My goal is to make modal editing more widespread outside of Vim. There is a Zen-like benefit to using a tool that does just what you tell it to – slicing and dicing text with surgical precision. My focus is not on adding new features to Kate (although I have implemented a few), but on sharpening existing functionality. In particular, I want to improve the integration between Kate&#8217;s vi mode and its extension system, as well as sort out various bugs pertaining to the vi mode.

Vegard has already done some really great work, and his changes should be trickling in to Kate&#8217;s git repository the next weeks. You can follow his work at [http://quickgit.kde.org/index.php?p=clones%2Fkate%2Fvegardoye%2Fvegard\_gsoc\_2012.git][2].

 [1]: /wp-content/uploads/2012/06/300x400.jpg
 [2]: http://quickgit.kde.org/index.php?p=clones%2Fkate%2Fvegardoye%2Fvegard_gsoc_2012.git