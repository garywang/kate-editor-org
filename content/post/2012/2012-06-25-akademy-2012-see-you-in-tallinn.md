---
title: Akademy 2012 – See you in Tallinn ;)
author: Christoph Cullmann

date: 2012-06-25T18:35:51+00:00
url: /2012/06/25/akademy-2012-see-you-in-tallinn/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
One year more passed and the next <a href="http://akademy2012.kde.org/" title="Akademy 2012" target="_blank">Akademy</a> will soon open its doors.

Was never before in <a title="Tallinn" href="http://en.wikipedia.org/wiki/Tallinn" target="_blank">Tallinn</a> or <a title="Estonia" href="http://en.wikipedia.org/wiki/Estonia" target="_blank">Estonia</a>, will be an interesting visit.

Hope to meet again a lot of old and new KDE people.

Didn&#8217;t code a lot at all since last years meeting, too busy to finalize my phd thesis.  
Hope to get more time for Kate in the next months again ;) And at the meeting.

<p style="text-align: left;">
  <a href="http://akademy2012.kde.org/"><img class="size-medium wp-image-1823" title="Ak2012_imgoing2" src="/wp-content/uploads/2012/06/Ak2012_imgoing2-300x133.png" alt="" width="300" height="133" srcset="/wp-content/uploads/2012/06/Ak2012_imgoing2-300x133.png 300w, /wp-content/uploads/2012/06/Ak2012_imgoing2.png 400w" sizes="(max-width: 300px) 100vw, 300px" /></a>
</p>