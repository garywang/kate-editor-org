---
title: Project Management, Take Three
author: Christoph Cullmann

date: 2012-08-12T19:32:18+00:00
url: /2012/08/12/project-management-take-three/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
After bit more playing around with the project plugin, I did get aware that I am even to lazy to open project files. Normally if I need to fix something or add new features, I just go to the directory and open the source or build system files I need to work on.  Therefore the project plugin now just watches which files you open and will load the corresponding project on the fly for you ;) (instead of naming your project, you create a .kateproject hidden file in the toplevel project dir, just like Git would do it with its .git directory)

Next issue that arised: even with the simple pattern syntax like:

<pre>"files": [ { "directory": "kate", "filters": ["*.cpp", "*.h", "*.ui", "CMakeLists.txt", "Find*.cmake"], "recursive": 1 } ]</pre>

it is hard to enumerate all files in the kate.git, as we have .py files, .xml files, .desktop files, .sh files and so on.

As I started here locally to add more and more extensions, I got the impression that just asking Git would be an better idea, and voila:

<pre>"files": [ { "directory": "kate", "git": 1, "recursive": 1 } ]</pre>

and you get recursively all files that are registered in your Git clone in the kate directory ;)

If you still have SVN, no problem, for your project this will work:

<pre>"files": [ { "directory": "kate", "svn": 1, "recursive": 1 } ]</pre>

These extensions make creating your Kate project much easier I guess and avoid any battling with regular expressions to keep track of the important files.

Other improvements:

  * Current file will be selected correctly in the projects view
  * Search and Replace is enhanced with an &#8220;in Project&#8221; option, if the project plugin is loaded and any project active, for this a bit work was needed to allow inter-plugin communication ;)

And again, the mandatory screenshot:

[<img class="aligncenter size-large wp-image-1953" title="Kate Project, now with searching" src="/wp-content/uploads/2012/08/kateproject_search-1024x691.png" alt="" width="770" height="519" srcset="/wp-content/uploads/2012/08/kateproject_search-1024x691.png 1024w, /wp-content/uploads/2012/08/kateproject_search-300x202.png 300w, /wp-content/uploads/2012/08/kateproject_search.png 1078w" sizes="(max-width: 770px) 100vw, 770px" />][1]

 [1]: /wp-content/uploads/2012/08/kateproject_search.png