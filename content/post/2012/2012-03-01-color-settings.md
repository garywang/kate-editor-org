---
title: Color Settings
author: Dominik Haumann

date: 2012-03-01T10:28:44+00:00
url: /2012/03/01/color-settings/
pw_single_layout:
  - "2"
categories:
  - Developers
  - Users
tags:
  - planet

---
This week, Kate Part received an update of the &#8220;Colors&#8221; tab in the settings dialog, available in KDE 4.9. The major features include

  * configurable colors: search highlight, replace highlight and [modified line colors][1] &#8211; finally :-)
  * possibility to always use colors from the KDE color scheme.
  * the implementation works in a way that at some point, the host application (KDevelop, Kile, &#8230;) can put items into this list.

Here are screenshots of the new (top) and the old (bottom) color tab:

[<img class="alignnone size-full wp-image-1712" title="New Color Tab" src="/wp-content/uploads/2012/03/colors-new.png" alt="" width="904" height="560" srcset="/wp-content/uploads/2012/03/colors-new.png 904w, /wp-content/uploads/2012/03/colors-new-300x185.png 300w" sizes="(max-width: 904px) 100vw, 904px" />][2]

[<img class="alignnone size-full wp-image-1713" title="Old Color Tab" src="/wp-content/uploads/2012/03/colors-old.png" alt="" width="904" height="579" srcset="/wp-content/uploads/2012/03/colors-old.png 904w, /wp-content/uploads/2012/03/colors-old-300x192.png 300w" sizes="(max-width: 904px) 100vw, 904px" />][3]

The new implementation uses a QTreeWidget along with a custom delegate to draw the colors button and reset icon. It would be nice to have an appearance of the categories like in systemsettings, or like in dolphin. Dolphin probably uses a KCategorizedView, which in turn uses KCategoryDrawer. KCategoryDrawer could be used, but needs fine tuning in order to make it look nice. Does someone know how systemsettings draws the nice shaded backgrounds of the categories?

 [1]: /2011/09/06/line-modification-system/ "Kate Modification Markers"
 [2]: /wp-content/uploads/2012/03/colors-new.png
 [3]: /wp-content/uploads/2012/03/colors-old.png