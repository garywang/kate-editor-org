---
title: Scripting Kate
author: Dominik Haumann

date: 2009-11-01T15:04:00+00:00
url: /2009/11/01/scripting-kate/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2009/11/scripting-kate.html
categories:
  - Developers
  - Users

---
In my <a href="/2009/10/29/extending-kate-with-scripts/" target="_self">last blog</a> I explained Kate&#8217;s scripting features in KDE 4.4. To better understand how scripting can be used let&#8217;s look at some use cases.

  * [join lines][1]: This feature request wants the action &#8220;join lines&#8221; to not join different paragraphs, i.e. not remove empty lines. We have not implemented this wish, as there are probably users who prefer the current behaviour. This request can be fixed by writing a small script that joins the lines according to the user&#8217;s wishes.
  * [reformat paragraph][2]: An intelligent reformatter for paragraphs. Should be rather straight forward to implement.
  * XML tools: In KDE3, Kate once had a xmltools plugin. Unfortunately noone ported it to KDE4. The plugin provided lots of very useful features for xml editing. For example, you could select text and then wrap it with xml elements, e.g. &#8220;text&#8221; would become &#8220;<para>text</para>&#8221;. This is a perfect example for a command line script as well. Any volunteers? :)

Scripting also brings us closer to fixing the following reports:

  * [macro system][3]: Kate still does not have a macro system. A macro can be interpreted as a group of scripts, executed in a sequence (more or less). The vi input mode already supports pretty complex commands, and the code for scripting is all there. It&#8217;s just a matter of putting this together so it&#8217;s usable for users.
  * [word count][4]: maybe the word count features can be implemented by a script (too slow?). Problem is, that a script cannot show dialogs etc.

To make scripting an even better experience, we still need to implement binding shortcuts to scripts. Again: any volunteers? :)

 [1]: https://bugs.kde.org/show_bug.cgi?id=125104
 [2]: https://bugs.kde.org/show_bug.cgi?id=93398
 [3]: https://bugs.kde.org/show_bug.cgi?id=44908
 [4]: http://bugs.kde.org/show_bug.cgi?id=65740