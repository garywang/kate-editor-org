---
title: Kate/KDevelop Sprint – First Weekend
author: Christoph Cullmann

date: 2014-01-20T13:52:58+00:00
url: /2014/01/20/kate-kdevelop-sprint-first-weekend/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet
  - sprint

---
Here we sit, in Barcelona, hacking away at KTextEditor & Kate.  
During the first 2 days, we already got some stuff done, like cleaning up KTextEditor interfaces and port more parts to KF5 (like ctags plugin, sql plugin, &#8230;).

More and more, KTextEditor & Kate get into a usable state for frameworks ;)  
It is really good to have some free days in a nice location to focus on that goal!

We will not fundamentally change a lot of stuff but really concentrate on long term items we had on the radar for the 4.x => 5.x change and on having a GOOD 5.0 release that is as usable as the 4.x series with hopefully only as few regressions as possible.