---
title: Kate needs you!
author: Christoph Cullmann

date: 2014-09-23T19:54:26+00:00
url: /2014/09/23/kate-needs-you/
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
The next major step in Kate&#8217;s evolution is close: Kate based on KDE Frameworks 5.

Whereas it already works well enough for me (and others), it would be nice to clear out as many issues as possible before we have our first official KF 5 based release.

Our Bugzilla is full with smaller and larger Kate/KTextEditor (aka KatePart) issues, see:

  * <a title="Kate Bugs" href="https://bugs.kde.org/buglist.cgi?product=frameworks-ktexteditor&product=kate&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor" target="_blank">Our bugs</a> in the KDE bugtracker
  * <a title="Kate Wishes" href="https://bugs.kde.org/buglist.cgi?product=frameworks-ktexteditor&product=kate&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist" target="_blank">Our wishes</a> in the KDE bugtracker

Whereas Kate/KTextEditor has people working on it and continue to improve it over time, we don&#8217;t have enough people to keep track and care for all our reported bugs/wishs.

Therefore, if you have a clue about Qt and if you use Kate or any application using our KTextEditor editing component, like KDevelop, Kile, &#8230;. => Think about helping us out.

<a title="KDE Frameworks 5 Building HowTo" href="https://community.kde.org/Frameworks/Building" target="_blank">Here</a> is a nice guide how to build KF 5 stuff.

We are happy to review your patches, any help is welcome!

(And yes, shame on me, during Akademy I found patches older than one year hanging around on Bugzilla, now applied, and still some controversial ones are floating around.)

Even if you are no developer, reviewing bugs/wishs and killing off issues that got already solved or are not applicable would already be a great help!