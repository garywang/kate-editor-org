---
title: Akademy 2014 – Soon ;=)
author: Christoph Cullmann

date: 2014-09-01T18:12:41+00:00
url: /2014/09/01/akademy-2014-soon/
categories:
  - Events
  - KDE
tags:
  - planet

---
Only some days until the yearly <a title="Akademy 2014" href="https://akademy.kde.org/2014" target="_blank">Akademy</a> starts.

Its a real great thing to get known to other KDE/Qt contributors and meet old and new friends.

Already amazed, lets hope the airlines don&#8217;t go on strike during my travel :=)

See you all there, lets have fun & be productive! Already now thanks to all people that help to organize this cool event and all our <a title="Sponsors" href="https://akademy.kde.org/2014/sponsors" target="_blank">sponsors</a>!  
[<img class="aligncenter size-full wp-image-3371" alt="Banner400.going" src="/wp-content/uploads/2014/09/Banner400.going_.png" width="400" height="178" srcset="/wp-content/uploads/2014/09/Banner400.going_.png 400w, /wp-content/uploads/2014/09/Banner400.going_-300x133.png 300w" sizes="(max-width: 400px) 100vw, 400px" />][1]

 [1]: /wp-content/uploads/2014/09/Banner400.going_.png