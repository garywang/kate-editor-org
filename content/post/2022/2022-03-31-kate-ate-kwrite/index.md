---
title: Kate ate KWrite
author: Christoph Cullmann
date: 2022-03-31T21:45:00+02:00
url: /post/2022/2022-03-31-kate-ate-kwrite/
---

# The current state up to KDE Gear 22.04

Kate & KWrite always existed as pair in the last 20 years.

KWrite was there first, a SDI editor already shipped with very early KDE versions.

Kate was started by me to have a MDI variant of KWrite.

KWrite was kept untouched, more or less, over the last 20 years beside minor improvements and bug fixes.

Naturally a lot features slipped in due to the fact that it uses KTextEditor as editor component.

But in principle, KWrite today is more or less like KWrite 20 years ago.

This now changes :)

It all started after Waqar [started to implement](https://invent.kde.org/utilities/kate/-/merge_requests/691) the long wanted tabbing support in KWrite.

(As a side note, Waqar rocks, he did implement a *large* amount of great stuff in the last year and fixed plenty of bugs, thanks a lot!)

Whereas there was nothing wrong with the new code, I was unhappy with this approach.

We re-implemented tabbing in Kate I think at least three times in the main application and even before as at least two different plugins.

I didn't want to have yet-another-tabbing implementation in our repository that then will need again all the love we gave to the current on in Kate.

(And yes, even such a "trivial" feature is a lot of work, do you want close button? do you want to hide them? what happens if you click save in a new document in a new tab, which directory is choosen?...)

# The future as in KDE Gear 22.08

This leads to the solution for this issue:

Don't have a own code base for KWrite at all, just re-use the Kate code base and deactivate some features.

I [started to work on this](https://invent.kde.org/utilities/kate/-/merge_requests/692) directly after above request was discussed.

The idea was:

Create a shrinked down Kate without sessions and any plugins.

As can be seen in the linked [merged request](https://invent.kde.org/utilities/kate/-/merge_requests/692) this actually is a lot more trivial then thought.

As Kate without any plugins is already very simple, just by deactivating the session stuff and hiding a few more advanced features, on can nicely emulate a more simple editor.

The Kate and KWrite code base now just differ by the used main function and a few if checks inside the shared code if we are in Kate or KWrite mode.

This actually allowed to shrink down the code base by more or less the complete 1k lines of code we had previously in KWrite without adding more than perhaps 5 lines of code to the shared part.

Even the main functions have now a lot more sharing, as the shared code does the most heavy lifting, even most parts of the command line parsing.

Thanks to Waqar and Nate for reviewing the changes and providing feedback what should be kept/removed.

I hope this is a really nice change for our users.

But don't be mistaken, KWrite is still not Kate.

It will behave a lot more like KWrite did, no shared instance, no sessions, no fancy plugins, therefore no terminal, LSP, ....

If you want all the fancy stuff, install Kate, if you have just basic editing needs, install KWrite.

Or if you need both, install both of them.

They neither depend on each other as package nor do they clash, just like before.

They will not mess up each other settings either, they will behave like proper twins.

Below screenshots of both applications taken from the current master branch that will be the base for 22.08 (started without any existing configuration with the same file in the kate.git as argument).

![Preview of Kate 22.08](/post/2022/2022-03-31-kate-ate-kwrite/images/kate-22.08-preview.png)

![Preview of KWrite 22.08](/post/2022/2022-03-31-kate-ate-kwrite/images/kwrite-22.08-preview.png)

The only direct difference between Kate & KWrite are the missing tool views and the per default enabled or disabled toolbar & urlbar.

Beside this and the missing features the plugins provides and the session management, all UI elements that exist in both applications are 1:1 identical.

The tabbing behaves line in Kate, too, you have quick open, splitting, etc.

You just lack all advanced features that are outsources to the various plugins and the session management (including the more MDI style instance sharing).

I hope this will make people happy that wanted a long time to have more advanced stuff in KWrite but not the full feature set Kate provides.

You can still use KWrite nicely to pipe in logs, keep notes, etc.
It will more or less behave like before, just with tabs and a few more bells and whistles.

And both applications will directly benefit from every bug fix we do in the future, without any code duplication.

# Help wanted!

Naturally, even after Kate ate KWrite and we have a lot less code to maintain, there is still a lot of stuff that can be improved.

If you want to help us out, show up and [contribute](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/).

We are always searching for more help to make both our editors (and the underlying frameworks) even more outstanding!

# Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/tt80rh/kate_ate_kwrite/).
