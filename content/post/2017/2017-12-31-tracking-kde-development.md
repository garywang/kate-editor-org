---
title: Tracking KDE Development
author: Dominik Haumann

date: 2017-12-31T18:00:27+00:00
url: /2017/12/31/tracking-kde-development/
categories:
  - Developers
  - Users
tags:
  - planet

---
A central place to track KDE development is the [kde-commits@kde.org][1] mailing list. To this mailing list, _all_ code changes are sent, containing the log message as well as the diff that quickly shows what really changed. In the early days of KDE development, the changes were maintained by the CVS version control system. Later, this was changed to subversion. Nowadays, KDE mostly uses git, but some svn modules are still around.

Since KDE is developed by many contributors, the  kde-commits@kde.org mailing list obviously has high traffic, ranging from ~100 mails up to 400 mails a day (see [marc.info][2] for statistics).

Many developers are subscribed to this mailing list. However, due to high traffic and possibly many changes that are unrelated to your pet project, a commit filter was invented, which was available on https://commitfilter.kde.org for a long time. Essentially, this commit filter allowed to easily setup filters in terms of regular expressions, such that you&#8217;d only get mails for changes you are interested in. While this was convenient, the commit filter also had its drawbacks: Instead of getting all KDE changes, you missed a lot possibly interesting conversations: Since post-code reviews are often done by others through email conversation directly on the code changes.

Nowadays, the commit filter is not available anymore, mostly for security reasons since it was unmaintained for a long time.

What does that mean? I&#8217;m subscribed to [kde-commits@kde.org][1] again, getting all changes. In fact, I am enjoying skimming quickly through the changes, even doing a code review here and there.

I recommend every KDE contributor to subscribe to this mailing list, since it is very interesting to see which projects are actively developed and who really contributes to KDE in person. Even better, sometimes there are discussions on this list, helping you to learn things you didn&#8217;t know. Of course, nowadays we also have many code reviews on [phabricator][3], but still the commit mailing list is a nice addon to track KDE development.

 [1]: https://mail.kde.org/mailman/listinfo/kde-commits
 [2]: https://marc.info/?l=kde-commits
 [3]: https://phabricator.kde.org/