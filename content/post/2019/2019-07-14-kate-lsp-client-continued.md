---
title: Kate LSP Client Continued
author: Christoph Cullmann

date: 2019-07-14T13:33:00+00:00
excerpt: |
  The new LSP client by Mark Nauwelaerts made nice progress since the LSP client restart post last week.
  Reminder: The plugin is not compiled per default, you can turn it on via:
  
  cmake -DCMAKE_INSTALL_PREFIX=&ldquo;your prefix&rdquo; -DENABLE_LSPCLIENT=...
url: /posts/kate-lsp-client-continued/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/kate-lsp-client-continued/
syndication_item_hash:
  - 8a709e9c4b87c40ef9c2fd6e2cfd1f6b
categories:
  - Common

---
The new LSP client by Mark Nauwelaerts made nice progress since the [LSP client restart][1] post last week.

Reminder: The plugin is not compiled per default, you can turn it on via:

> cmake -DCMAKE\_INSTALL\_PREFIX=&ldquo;your prefix&rdquo; -DENABLE_LSPCLIENT=ON &ldquo;kate src dir&rdquo;

The code can still be found kate.git master, see [lspclient][2] in the addons directory.

What is new?

  * Diagnostics support: A tab in the LSP client toolview will show the diagnistics, grouped by file with links to jump to the locations. Issues will be highlighted in the editor view, too. 
    
    <p align="center">
      <a href="https://cullmann.io/posts/kate-lsp-client-continued/images/kate-diagnostics.png" ><img width=500 src="https://cullmann.io/posts/kate-lsp-client-continued/images/kate-diagnostics.png"></a>
    </p>

  * Find references: Find all references for some variable/function in your complete program. They are listed like the diagnostics grouped per file in an extra tab. 
    
    <p align="center">
      <a href="https://cullmann.io/posts/kate-lsp-client-continued/images/kate-find-references.png" ><img width=500 src="https://cullmann.io/posts/kate-lsp-client-continued/images/kate-find-references.png"></a>
    </p>

  * Improved document highlight: Highlight all occurrences of a variable/&hellip; inside the current document. Beside highlighting the reads/writes/uses, you get a jump list like for the other stuff as tab, too. 
    
    <p align="center">
      <a href="https://cullmann.io/posts/kate-lsp-client-continued/images/kate-document-highlight.png" ><img width=500 src="https://cullmann.io/posts/kate-lsp-client-continued/images/kate-document-highlight.png"></a>
    </p>

A feature I missed to show last time:

  * Hover support: Show more meta info about a code location, like the proper type, useful e.g. for [almost-always-auto][3] C++ programming. <p align="center">
      <a href="https://cullmann.io/posts/kate-lsp-client-continued/images/kate-hover.png" ><img width=500 src="https://cullmann.io/posts/kate-lsp-client-continued/images/kate-hover.png"></a>
    </p>

We even got already two patches for the fresh plugin:

  * [D22348 - Use the label when insertText or sortText is missing][4]
  * [D22349 - Don&rsquo;t send the Content-Type header][5]

Both are aimed to improve the support of the Rust LSP server. As you can see, they got already reviewed and merged.

Feel welcome to show up on <kwrite-devel@kde.org> and help out! All development discussions regarding this plugin happen there.

If you are already familiar with Phabricator, post some patch directly at [KDE&rsquo;s Phabricator instance][6].

You want more LSP servers supported? You want to have feature X? You have seen some bug and want it to vanish? => Join!

 [1]: https://cullmann.io/posts/kate-lsp-client-restart/
 [2]: https://cgit.kde.org/kate.git/tree/addons/lspclient
 [3]: https://herbsutter.com/2013/08/12/gotw-94-solution-aaa-style-almost-always-auto/
 [4]: https://phabricator.kde.org/D22348
 [5]: https://phabricator.kde.org/D22349
 [6]: https://phabricator.kde.org/differential/