---
title: Kate got submitted to the Windows Store
author: Christoph Cullmann
date: 2019-09-12T14:25:00+02:00
url: /post/2019/2019-09-12-kate-windows-store-submission/
---

Since a few years Kate is working well on Windows.
You can grab fresh installers since then from our [download page](/get-it).

But the visibility of it was very low for Windows users.

Already [last year](https://phabricator.kde.org/T9577) the plan was made to increase the visibility of KDE applications by getting them into the [official Windows Store](https://store.microsoft.com).

Like always, [Akademy](https://akademy.kde.org) is a great chance to get the last bits ironed out and the stuff done!

Now, thanks to the help of Hannah von Reth and others, we finally got Kate submitted to the Windows Store!

<p align="center">
    <a href="/post/2019/2019-09-12-kate-windows-store-submission/images/kate-windows-store-submission.png" target="_blank"><img width=500 src="/post/2019/2019-09-12-kate-windows-store-submission/images/kate-windows-store-submission.png"></a>
</p>

The submission still needs to be processed by Microsoft, stay tuned when our nifty editor will really be available there for download!

Thanks to all people that contributed to this!
