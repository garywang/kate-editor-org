---
title: Kate D-Bus Interfaces, used at all?
author: Christoph Cullmann

date: 2013-04-05T22:35:49+00:00
url: /2013/04/06/kate-d-bus-interfaces-used-at-all/
pw_single_layout:
  - "1"
categories:
  - Developers
  - KDE
  - Users
tags:
  - planet

---
I just started to clean up the content of kate.git, moving things around, fixing compile warnings and similar stuff.

I stumbled over warnings in our dbus interfaces.

The main use of them is to allow Kate to reuse an existing instance for opening files and sessions. This part (e.g. the interface of the application object itself) works fine and is needed.

But all other interfaces, like the ones for docmanager, mainwindows, &#8230; are mostly non-existant or not implemented. I now play with the idea of just removing the nearly empty skeleton implementations, as it seems nobody missed them during the complete 4.x series.

Is there somebody out there that actually uses them and if, how?