---
title: Downloading Kate Highlighting Files
author: Dominik Haumann

date: 2018-08-14T12:58:03+00:00
url: /2018/08/14/downloading-kate-highlighting-files/
categories:
  - KDE
  - Users
tags:
  - planet

---
Starting with the KDE Frameworks 5.50 release we decided to remove the capability in Kate/KTextEditor to download / update syntax highlighting files from the Kate homepage.

The reasons for this are as follows:

  1. The KTextEditor framework is released once a month, meaning that users who use latest KDE software usually anyways have the most recent versions. Other users who do not follow the latest development releases (like your mom) are likely not the target audience for downloading highlighting files.
  2. There are technical problems with only updating certain highlighting files, since it can lead to an inconsistent state when one highlighting file needs another one that was not updated or does not exist, or also if a highlighting file needs a certain indenter that does not yet exist or contains bugs.
  3. We have a nice small cleanup in the UI, since we have now one button less.

Git changes:

  * Code: <https://cgit.kde.org/ktexteditor.git/commit/?id=705f8e0419c441ecb618d35b70b4373be453a94e>
  * Documentation: <https://cgit.kde.org/kate.git/commit/?id=c07523227d1cd3edd756978a6c48d9857963f705>

**Contributing new Highlighting Files**

By the way, we are always very happy to accept new highlighting files under MIT license. Documentation about how to write a syntax highlighting file can be found in the [Kate Handbook][1]. When you are done, please contribute your highlighting file in [phabricator][2] for the &#8216;syntax-highlighting&#8217; framework (click Code Review, then on the very top right &#8220;Create Diff&#8221;). You can find nice instructions also on the [Community wiki][3].

 [1]: https://docs.kde.org/stable5/en/applications/katepart/highlight.html
 [2]: https://phabricator.kde.org
 [3]: https://community.kde.org/Infrastructure/Phabricator