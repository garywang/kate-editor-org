---
title: Article about cmake
author: Dominik Haumann

date: 2018-02-21T22:21:19+00:00
url: /2018/02/21/article-about-cmake/
categories:
  - Developers
  - KDE
tags:
  - planet

---
Just in case someone is interested: two days ago a very good article about cmake popped up, called [It&#8217;s Time to do CMake Right][1]. There also is a discussion about it [on reddit/r/cpp][2]. happy reading :)

 [1]: https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/
 [2]: https://www.reddit.com/r/cpp/comments/7yps20/its_time_to_do_cmake_right/