---
title: Kate in KDE 4.7
author: Dominik Haumann

date: 2011-07-09T20:20:50+00:00
url: /2011/07/09/kate-in-kde-4-7/
categories:
  - Developers
  - Users
tags:
  - planet

---
Now that the next version of the KDE platform is branched, it&#8217;s time to have a look at all the changes in Kate in KDE 4.7.

  * **More than 150 wishes and issues have been resolved**. A <a title="Resolved Issues in KDE 4.7" href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=kate&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&resolution=FIXED&resolution=WORKSFORME&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2010-12-29&chfieldto=2011-07-31&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0=" target="_blank">detailed list of the resolved  issues</a> can be found in the KDE bug tracker.
  * All Kate development is done in the <a title="Kate Git Module" href="https://projects.kde.org/projects/kde/kdebase/kate/repository" target="_blank">KDE git infrastructure</a> as submodule of kdebase. It is very straight forward to [build Kate within a few minutes][1] according to our excellent documentation.
  * New [Search Plugin][2] with the ability to search in files on disk or opened files (will replace the Find in Files plugin in KDE 4.8).
  * Plugin improvements: [Build Plugin][3]., [GDB Plugin][4]
  * Kate&#8217;s [tab bar plugins][5] gained several new features.
  * Swap files: If lost data is found, documents are marked as read-only.
  * Further noteworthy changes: [Improved printing][6], [change of line ending triggers modified flag][7], [fixed shortcut issues][8], <a title="Auto Completion Popup" href="https://bugs.kde.org/show_bug.cgi?id=264757" target="_blank">fixed auto completion popup</a>, and more.

Current State of Kate

  * In KDE 4.6, we replaced the document list by the new [File Tree][9]. We got relatively few critical reports, meaning that the file tree seems to work pretty well for the majority.
  * Kate has two GSoC projects at the moment: Rewrite of the code folding, and Improving Kate&#8217;s vi mode. The work done in both projects will be available in KDE 4.8, so we are already working on the next major KDE release.
  * About 6 Kate developers are present at the <a title="KDE Conference Website" href="http://akademy.kde.org" target="_blank">annual KDE conference</a>.
  * [A year ago][10], the Kate homepage was redesigned. Looking back the last year, we can say that this has been a success.

Just in case you missed it: you can read about [Kate&#8217;s state 6 months ago here][11].

 [1]: /get-it/ "Build Kate"
 [2]: /2011/06/28/kate-plugin-updates-part-3/ "Search Plugin in KDE 4.7"
 [3]: /2011/06/21/kate-plugin-updates-part-1/ "Build Plugin in KDE 4.7"
 [4]: /2011/06/23/kate-plugin-updates-part-2/ "GDB Plugin in KDE 4.7"
 [5]: /2011/04/25/kates-tab-bar-plugins/ "Kate's Tab Bar Plugins"
 [6]: https://bugs.kde.org/show_bug.cgi?id=200248 "Kate Printing"
 [7]: https://bugs.kde.org/show_bug.cgi?id=143120 "Line Ending"
 [8]: https://bugs.kde.org/show_bug.cgi?id=144945 "Shortcut Issues"
 [9]: /2010/09/10/tree-view-plugin-introduction/ "File Tree"
 [10]: /2010/08/14/kate-editor-org-moved/ "Kate Homepage Moving"
 [11]: /2010/12/29/kate-in-kde-4-6/ "Kate in KDE 4.6"