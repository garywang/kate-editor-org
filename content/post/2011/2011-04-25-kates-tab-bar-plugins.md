---
title: Kate’s Tab Bar Plugins
author: Dominik Haumann

date: 2011-04-25T14:30:59+00:00
url: /2011/04/25/kates-tab-bar-plugins/
categories:
  - Users
tags:
  - planet

---
If you check Kate&#8217;s plugin list, you will recognize that there are two different tab bar plugins available. The first one is **Tabify**. This plugin adds a standard KDE tab bar to the top of Kate&#8217;s main window:<img class="aligncenter size-full wp-image-875" title="Tab Bar" src="/wp-content/uploads/2011/04/tabbar.png" alt="" width="667" height="359" srcset="/wp-content/uploads/2011/04/tabbar.png 667w, /wp-content/uploads/2011/04/tabbar-300x161.png 300w" sizes="(max-width: 667px) 100vw, 667px" />

The plugin shows the same entries as the &#8220;Documents&#8221; tool view on the left. With drag&drop, the tabs can be moved around. In KDE SC <= 4.6.2 this plugin is called Tabify. In KDE SC >= 4.6.3 the name is simply &#8220;Tab Bar&#8221;.

There is another plugin, called Tab Bar Extension in KDE SC <=4.6.2 (and Multiline Tab Bar in KDE SC >=4.6.3) that also adds a tab bar to Kate&#8217;s mainwindow:<img class="aligncenter size-full wp-image-876" title="Multiline Tab Bar" src="/wp-content/uploads/2011/04/multiline-tabbar.png" alt="" width="637" height="329" srcset="/wp-content/uploads/2011/04/multiline-tabbar.png 637w, /wp-content/uploads/2011/04/multiline-tabbar-300x154.png 300w" sizes="(max-width: 637px) 100vw, 637px" />

As can be seen, this tab bar can be configured to span multiple lines in order to show more documents at the same time. Further, there are features like tab highlighting so that a document can quickly be found when working on a huge list of documents. There are more options in the configure dialog of this plugin (click small configure button on the right). In KDE 4.7, it is possible to change to highlight a tab by clicking on it with the middle mouse button and clear the highlight with CTRL+middle-click.

There have been requests to remove the non-standard plugin from Kate and just provide the standard conform Tabify plugin. I was about to do that, but then, we do have users that use the multiline tab bar. And as it works well and provides some features the default tab bar does not have, I&#8217;ve finally decided to keep it.

It is worth to mention that there were quite a lot of voices in the past requesting a tab bar. It was even requested to remove the &#8220;Documents&#8221; tool view, listing all the documents. However both tab bar plugins are not able to provide easy access to the opened documents, if you have e.g. 50 documents opened. Hence, the Documents tool view will remain the default, just as it always was in Kate&#8217;s life :)