---
title: You, KDE!
author: Dominik Haumann

date: 2011-07-25T15:28:16+00:00
url: /2011/07/25/you-kde/
categories:
  - KDE
tags:
  - planet

---
For me, it (still) feels very unnatural to <a title="KDE" href="http://michael-jansen.biz/blog/mike/2011-07-25/kde-subversion-stampede" target="_blank">talk about us as KDE</a>. I&#8217;m still thinking in terms of _the KDE community_; and _the KDE Project_ releases the _KDE Desktop_ (or just KDE). I&#8217;m also fine with _the KDE Workspace_ and other specialized variants. However, the login manager now also shows the term _KDE Plasma Workspace_. In my very own humble opinion, this is already too much of buzz words.

At university, I&#8217;m helping out with system administration. And since I&#8217;m very familiar with KDE, I fix all KDE related issues (and usually enjoy doing so). We have lots of colleagues that never used KDE. Now, try to explain the term Plasma&#8230; Well, it&#8217;s a desktop shell &#8211; also too complex. It&#8217;s the desktop, with all the icons and panels. That&#8217;s more understandable :) But next, a dialog pops up with some status messages: Akonadi. Well, try to explain the term Akonadi. It&#8217;s similar with other technologies. Explain what Nepomuk is doing with the difference to Strigi&#8230; When I try to explain what&#8217;s happening, and what all these components do, I sometimes feel <del>pretty much stupid</del> uncomfortable using these words.

It is similar with the term KDE SC. As part of the rebranding, the KDE Software Compilation or short KDE SC was introduced. I can understand that we build a platform and our target is more than just the Desktop. But talking to other people, I feel stupid saying _KDE SC_. For me, it simply is (and probably will always be) a no-go. But it changed already, to _the KDE platform_, or maybe I misunderstood that ? :-)

Funnily, I think it&#8217;s fine to use application names such as dolphin, kate, amarok and what not. So to some degree, I&#8217;m certainly simply used to these terms. But still, _it feels right_, also when talking to other users. We have to be very careful to use fancy names in very prominent places. Ah, and with respect to naming applications, there is a similar <a title="System Settings" href="http://lists.kde.org/?l=kde-core-devel&m=131132292207915&w=2" target="_blank">discussion on kde-core-devel</a> right now ;)