---
title: Getting Involved
author: Dominik Haumann

date: 2011-06-04T12:47:29+00:00
url: /2011/06/04/getting-involved/
categories:
  - Developers
tags:
  - planet

---
Recently we&#8217;ve had several [feature requests in comments of a blog post][1]. If you are interested, you can easily implement this and contribute to Kate. And I&#8217;ll even show you how to get started for the following feature. First, build Kate according to <a rel="nofollow" href="../get-it/">/get-it/</a>.

**Adding support for ctrl+w {left, right, up, down} to switch the active view.**

Kate has a [vi input mode][2]. This way, vim users can still use their default work flow but still use Kate at the same time. What&#8217;s missing in the current vi input mode implementation is support for view navigation. In vim, to move between different windows, press ctrl-w <arrow keys> to switch to the neighboring view. Implementing this is rather easy &#8211; the following steps need to be taken care of:

  1. Add vim bindings (=vim shortcuts) to the Kate&#8217;s implementation of the &#8216;normal mode&#8217;.
  2. In the implementation, find all visible KateViews.
  3. Compare the global coordinates of the window geometry by using <a title="QWidget::mapToGlobal" href="http://doc.qt.nokia.com/4.7/qwidget.html#mapToGlobal" target="_blank">QWidget::mapToGlobal(const QPoint& pos)</a> (for QPoint(0, 0) and QPoint(width(), height()) of all KateViews)
  4. Call setFocus to the nearest matching of the visible views.

[Patches][3] are welcome :)

 [1]: /2011/05/23/kates-folding-code-and-vi-mode-to-be-improved-by-gsoc-students/ "GSoC 2011 Students"
 [2]: /kate-vi-mode/ "Kate's Vi Input Mode"
 [3]: mailto:kwrite-devel@kde.org "Kate/KWrite mailing list"