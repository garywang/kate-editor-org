---
title: Kate Vi Input Mode – GSoC 2011
author: Svyatoslav Kuzmich

date: 2011-07-26T12:44:04+00:00
url: /2011/07/26/vimode-gsoc2011/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
<p style="text-align: justify;">
  I want to introduce you my GSoC-2011 project. I was working on improvement the vi input mode for the Kate part.
</p>

<p style="text-align: justify;">
  There are a lot of improvements for normal mode. Now it&#8217;s able to use jump list by pressing <strong>ctrl-i/ctrl-o</strong> in normal mode like in vim. And now not only registers but also marks, and jumps stored in the config of the session. <strong>H</strong>, <strong>M </strong>and <strong>L</strong> commands moves cursor to the first(<strong>H</strong>ome), <strong>M</strong>iddle and <strong>L</strong>ast line of the screen. To go to the any percent of the document you can use the <strong>%</strong> command by typing the number of percent(1-100) before it. <strong>gk </strong>and <strong>gj</strong> motions will make you able to go visual(display) lines up and down. It&#8217;s useful when you using line wrap.
</p>

<p style="text-align: justify;">
  You can split the screen vertically and horizontally by ctrl-s and ctrl-v shortcuts and move around them in different directions using <strong>ctrl-w{h,j,k,l,w}</strong> or also c<strong>trl-w{</strong><span style="font-family: Liberation Serif,serif;"><strong>←,↑,↓,→</strong></span><strong>}</strong>.
</p>

<p style="text-align: justify;">
  In insert mode added commands <strong>ctrl-r+[register]</strong> to insert the contents of a register and <strong>ctrl-o</strong> to switch to the normal mode only for a one command and automatically get back to insert. The<strong> ctrl-a</strong>, <strong>ctrl-x</strong> command now increase and decrease the negative numbers too.
</p>

<p style="text-align: justify;">
  There are also some new command-line features. Now you can use the marks for a command ranges. It&#8217;s possible to add and subtract line numbers in the ranges many times. For example command <strong>:.+3d</strong> will delete the third line after current position and the command <strong>:&#8217;a,&#8217;b-1y</strong> will yank the line-wise range between the line of mark &#8216;<strong>a&#8217;</strong> and the one line before the line of &#8216;<strong>b</strong>&#8216; mark.
</p>

<p style="text-align: justify;">
  Also there a some new basic vim commands:
</p>

<ul style="text-align: justify;">
  <li>
    <strong>:d</strong> or <strong>:delete</strong> – delete range;
  </li>
  <li>
    <strong>:c</strong> or <strong>:change</strong> – change range;
  </li>
  <li>
    <strong>:j</strong> – join all the lines in range;
  </li>
  <li>
    <strong>:></strong> &#8211; indent range;
  </li>
  <li>
    <strong>:<</strong> &#8211; unindent range;
  </li>
  <li>
    <strong>:y</strong> or <strong>:yank</strong> – yank range;
  </li>
  <li>
    <strong>:ma</strong> or <strong>:mark</strong> or <strong>:k</strong> – set mark;
  </li>
  <li>
    <strong>:bn</strong> – switch to next buffer(document);
  </li>
  <li>
    <strong>:bp</strong> – switch to previous buffer(document);
  </li>
  <li>
    <strong>:bf</strong> – to first buffer;
  </li>
  <li>
    <strong>:bl</strong> – to last buffer;
  </li>
  <li>
    <strong>:b [N]</strong> or <strong>:buffer [N]</strong> &#8211; to <strong>N</strong>th buffer
  </li>
</ul>

<p style="text-align: justify;">
  Text objects are also improved and fixed. There are new objects: ( <strong>a{</strong> , <strong>i{</strong> , <strong>a<</strong> , <strong>i<</strong> , <strong>a`</strong> , <strong>i` </strong>). And now they normally works with nested objects. So for now the <strong>i(</strong> object (for position of multiply symbol <strong>*</strong>) will be &#8220;(<span style="color: #ff0000;">1<strong>*</strong>2/(2+3)–4</span>)&#8221; instead of &#8220;(<span style="color: #ff0000;">1<strong>*</strong>2/(2+3</span>)–4)&#8221;. Some of the objects like <strong>(),{},[],<></strong> works with several lines.
</p>

<p style="text-align: justify;">
  Visual mode now is integrated with mouse, so the mouse dragging starts the visual mode. All kind of Kate-way selection is also starts the visual mode. For example if you use “<strong>Select all</strong>” menu line or shortcut <strong>ctrl-a</strong>(by default) this will starts visual mode with the selected text and waits you for a command to execute :) . One point mouse click will exit the visual mode. Kate selection and vi mode selection is actually the same thing now. So you can, say, to select a bloc by using the Visual Block mode (<strong>ctrl-v</strong>), to do a context menu on them and to copy them to a clipboard :). When you exit visual mode the <strong>&#8216;<</strong> and <strong>&#8216;></strong> marks have a start and end position of selection. It useful when you want to execute some command line command on the selected text. Just press the “<strong>:</strong>” when you select something and it automatically fill the command line with “<strong>&#8216;<,&#8217;></strong>” to have a quick access to selected area.
</p>

[<img class="size-full wp-image-1062 aligncenter" src="/wp-content/uploads/2011/07/foobar.png" alt="" width="346" height="32" srcset="/wp-content/uploads/2011/07/foobar.png 346w, /wp-content/uploads/2011/07/foobar-300x27.png 300w" sizes="(max-width: 346px) 100vw, 346px" />][1]

<p style="text-align: justify;">
  Vi marks and Kate bookmarks are integrated too. If you set the mark then this line becomes bookmarked by Kate. And vice versa: if you set a Kate&#8217;s bookmark and there no vi marks on this like vi mode sets the vim mark on this line and zero column, and shows you what letter for this mark is used.You can see the position of all vi marks in the bookmark menu in the menu bar or also in the context menu.
</p>

<p style="text-align: left;">
  <a href="/wp-content/uploads/2011/07/setting-bookmark1.png"><img class="size-full wp-image-1056 aligncenter" src="/wp-content/uploads/2011/07/setting-bookmark1.png" alt="" width="613" height="215" srcset="/wp-content/uploads/2011/07/setting-bookmark1.png 663w, /wp-content/uploads/2011/07/setting-bookmark1-300x105.png 300w" sizes="(max-width: 613px) 100vw, 613px" /></a>
</p>

<p style="text-align: left;">
  I hope this features will make you feel even more comfortable while using Kate. You can try it now in the git version of Kate.<br /> <strong>Any notice or bug report are welcome !</strong>
</p>

 [1]: /wp-content/uploads/2011/07/foobar.png