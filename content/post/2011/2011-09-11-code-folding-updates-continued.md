---
title: Code Folding Updates
author: Dominik Haumann

date: 2011-09-11T17:23:17+00:00
url: /2011/09/11/code-folding-updates-continued/
pw_single_layout:
  - "2"
categories:
  - Developers
  - Users
tags:
  - planet

---
<p style="text-align: left;">
  Christoph and me cleaned up the visualization of the code folding for KDE 4.8 a bit. In the snapshot, the left image shows the old behavior, and the right one shows the new behavior. The background highlighting appears as soon as you hover over the code folding bar. We hope you like it :) Mockups of how to make it even better are welcome, of course! You can <a title="Building Kate" href="/get-it/">try it by building Kate yourself</a>, if you want.<br /> <a href="/wp-content/uploads/2011/09/folding-updates.png"><img class="aligncenter size-full wp-image-1458" title="Code Folding Updates" src="/wp-content/uploads/2011/09/folding-updates.png" alt="Kate Code folding" width="952" height="595" srcset="/wp-content/uploads/2011/09/folding-updates.png 952w, /wp-content/uploads/2011/09/folding-updates-300x187.png 300w" sizes="(max-width: 952px) 100vw, 952px" /></a>
</p>