---
title: The Kate Text Editor in 2021
author: Christoph Cullmann
date: 2021-12-31T19:36:00+02:00
url: /post/2021/2021-12-31-kate-in-2021/
---

## COVID-19 Pandemic

Unfortunately, like [the year 2020](/post/2020/2020-12-31-kate-in-2020/), for many people this was again no easy year.
A lot of us and our families & friends are still directly or indirectly affected by the [COVID-19 pandemic](https://en.wikipedia.org/wiki/COVID-19_pandemic).
Like last year, I still hope the future will look less bleak now that vaccination is on the rise and perhaps we even have a bit luck with less deadly virus mutations in the future.

<p>
<center>
<img width="333" alt="Coronavirus. SARS-CoV-2" src="/post/2021/2021-12-31-kate-in-2021/images/Coronavirus._SARS-CoV-2.png"></a><br />
<a href="https://commons.wikimedia.org/wiki/File:Coronavirus._SARS-CoV-2.png">Alexey Solodovnikov (Idea, Producer, CG, Editor), Valeria Arkhipova (Scientific Сonsultant)</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons<br />
</center>
</p>

## Contributions to Kate in 2021

From Kate's development perspective this year looks fantastic.
If you track a bit the development via our [merge requests](/merge-requests/) overview page or even better participate yourself in our projects, you might have already noticed it ;=)

<p>
<center>
<a href="/images/mascot/history/kate-3000px.png"><img width="333" alt="The Kate Icon" src="/images/mascot/history/kate-3000px.png"></a><br />
</center>
</p>

### Accepted Merge Requests of 2021

- 346 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 176 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 144 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 24 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

Compare this to the state of the years 2020 and 2019 below.

### Accepted Merge Requests of 2020

- 87 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 49 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 132 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 2 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

### Accepted Merge Requests of 2019

- 45 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 6 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

Naturally 2019 is very unfair, as there only the Kate main & website repository did use GitLab, still, a nice increase in contributions even compared to 2020!

This doesn't even cover all work, smaller bug fixes are just pushed without extra request and some requests naturally are rather minimal while other are "the real thing", aka some big new feature.

I still think the switch to [GitLab](https://about.gitlab.com/) was a real booster for getting contributions in.
Whereas [Phabricator](https://www.phacility.com/phabricator/) was already a big step up from just [Review Board](https://www.reviewboard.org/), the current [invent.kde.org](https://invent.kde.org) GitLab instance is really a massive upgrade.

And this is not just something cool for new contributors more used to what the have on GitHub, it is a real bliss for maintainers like me, too.
You can easily read the diffs of many smaller merge requests on your phone or tablet even in your coffee break at work or if you like to sit around like myself often in some coffee shop ;)
You can even nicely trigger the merges from there, after a lot of hard work from our fantastic administrator team even now with proper GitLab CI integration that first will properly compile and test the stuff!

## Contributors of the year 2021

You can view more or less all contributors to Kate and Co. on our [teams page](/the-team/), at least so far they are visible out of the version history of the Git repositories or GitLab requests.
I think it is really impressive that 116 people contributed to our projects this year!
First time contributors in that time frame are marked bold.

<p>
<table>
<tr>
<td>Waqar Ahmed <!-- 650 --></td>
<td>Christoph Cullmann <!-- 561 --></td>
</tr>
<tr>
<td>Jan Paul Batrina <!-- 92 --></td>
<td>Kåre Särs <!-- 81 --></td>
</tr>
<tr>
<td>Mark Nauwelaerts <!-- 81 --></td>
<td>Jonathan Poelen <!-- 63 --></td>
</tr>
<tr>
<td>Ahmad Samir <!-- 55 --></td>
<td>Alexander Lohnau <!-- 45 --></td>
</tr>
<tr>
<td><b>Phu Nguyen</b> <!-- 31 --></td>
<td>Yuri Chornoivan <!-- 31 --></td>
</tr>
<tr>
<td>Dominik Haumann <!-- 30 --></td>
<td><b>Igor Kushnir</b> <!-- 29 --></td>
</tr>
<tr>
<td>Laurent Montel <!-- 26 --></td>
<td><b>Ilia Kats</b> <!-- 22 --></td>
</tr>
<tr>
<td>Carl Schwan <!-- 21 --></td>
<td>Méven Car <!-- 20 --></td>
</tr>
<tr>
<td><b>Krzysztof Stokop</b> <!-- 19 --></td>
<td><b>Marcell Fülöp</b> <!-- 19 --></td>
</tr>
<tr>
<td>Friedrich W. H. Kossebau <!-- 18 --></td>
<td>Nibaldo González <!-- 18 --></td>
</tr>
<tr>
<td>Nicolas Fella <!-- 18 --></td>
<td><b>Fabian Wunsch</b> <!-- 17 --></td>
</tr>
<tr>
<td>Alex Turbov <!-- 15 --></td>
<td>Gary Wang <!-- 15 --></td>
</tr>
<tr>
<td>Volker Krause <!-- 15 --></td>
<td><b>Jan Blackquill</b> <!-- 12 --></td>
</tr>
<tr>
<td>David Faure <!-- 11 --></td>
<td>Heiko Becker <!-- 11 --></td>
</tr>
<tr>
<td><b>Jack Hill</b> <!-- 9 --></td>
<td>Michal Humpula <!-- 9 --></td>
</tr>
<tr>
<td>David Bryant <!-- 7 --></td>
<td>Albert Astals Cid <!-- 6 --></td>
</tr>
<tr>
<td>Andreas Gratzer <!-- 6 --></td>
<td><b>Denis Doria</b> <!-- 6 --></td>
</tr>
<tr>
<td><b>artyom kirnev</b> <!-- 6 --></td>
<td><b>Denis Lisov</b> <!-- 5 --></td>
</tr>
<tr>
<td>Aleix Pol <!-- 4 --></td>
<td>Nate Graham <!-- 4 --></td>
</tr>
<tr>
<td>Robert Hoffmann <!-- 4 --></td>
<td><b>Daniele Scasciafratte</b> <!-- 3 --></td>
</tr>
<tr>
<td>David Edmundson <!-- 3 --></td>
<td><b>Francis Laniel</b> <!-- 3 --></td>
</tr>
<tr>
<td>Héctor Mesa Jiménez <!-- 3 --></td>
<td>Kai Uwe Broulik <!-- 3 --></td>
</tr>
<tr>
<td><b>Marco Rebhan</b> <!-- 3 --></td>
<td>Martin Walch <!-- 3 --></td>
</tr>
<tr>
<td>Milian Wolff <!-- 3 --></td>
<td><b>Mufeed Ali</b> <!-- 3 --></td>
</tr>
<tr>
<td>Pino Toscano <!-- 3 --></td>
<td><b>Thiago Sueto</b> <!-- 3 --></td>
</tr>
<tr>
<td><b>shenleban tongying</b> <!-- 3 --></td>
<td><b>Ada Sauce</b> <!-- 2 --></td>
</tr>
<tr>
<td><b>Amaury Bouchra Pilet</b> <!-- 2 --></td>
<td>Andreas Sturmlechner <!-- 2 --></td>
</tr>
<tr>
<td>Antonio Rojas <!-- 2 --></td>
<td><b>Arek Koz (Arusekk)</b> <!-- 2 --></td>
</tr>
<tr>
<td>Ben Cooksley <!-- 2 --></td>
<td>Christoph Roick <!-- 2 --></td>
</tr>
<tr>
<td><b>Christopher Yeleighton</b> <!-- 2 --></td>
<td><b>Daniel Tang</b> <!-- 2 --></td>
</tr>
<tr>
<td><b>Eugene Popov</b> <!-- 2 --></td>
<td>Fabian Kosmale <!-- 2 --></td>
</tr>
<tr>
<td>Weng Xuetian <!-- 2 --></td>
<td><b>William Wold</b> <!-- 2 --></td>
</tr>
<tr>
<td>Ömer Fadıl Usta <!-- 2 --></td>
<td><b>Alberto Salvia Novella</b> <!-- 1 --></td>
</tr>
<tr>
<td>Alex Richardson <!-- 1 --></td>
<td>Alexander Potashev <!-- 1 --></td>
</tr>
<tr>
<td>Allen Winter <!-- 1 --></td>
<td><b>Andreas Abel</b> <!-- 1 --></td>
</tr>
<tr>
<td>Andreas Cord-Landwehr <!-- 1 --></td>
<td><b>Andrey Karepin</b> <!-- 1 --></td>
</tr>
<tr>
<td>Anthony Fieroni <!-- 1 --></td>
<td><b>Boris Petrov</b> <!-- 1 --></td>
</tr>
<tr>
<td>Christoph Feck <!-- 1 --></td>
<td>Christophe Giboudeaux <!-- 1 --></td>
</tr>
<tr>
<td>Daan De Meyer <!-- 1 --></td>
<td>Daniel Levin <!-- 1 --></td>
</tr>
<tr>
<td><b>Daniel Sonck</b> <!-- 1 --></td>
<td>David Redondo <!-- 1 --></td>
</tr>
<tr>
<td>David Schulz <!-- 1 --></td>
<td><b>Felipe Kinoshita</b> <!-- 1 --></td>
</tr>
<tr>
<td>Frederik Schwarzer <!-- 1 --></td>
<td><b>Gustavo  Rodrigues</b> <!-- 1 --></td>
</tr>
<tr>
<td>Hannah von Reth <!-- 1 --></td>
<td>Heinz Wiesinger <!-- 1 --></td>
</tr>
<tr>
<td><b>Ismael Asensio</b> <!-- 1 --></td>
<td><b>Jakub Benda</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Janet Blackquill</b> <!-- 1 --></td>
<td><b>Javier Guerra</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Johnny Jazeix</b> <!-- 1 --></td>
<td><b>Jonathan Lopez</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Jonathan Marten</b> <!-- 1 --></td>
<td><b>Karthik Nishanth</b> <!-- 1 --></td>
</tr>
<tr>
<td>Kevin Funk <!-- 1 --></td>
<td><b>Ksofix Alert</b> <!-- 1 --></td>
</tr>
<tr>
<td>Luigi Toscano <!-- 1 --></td>
<td><b>Luis Taira</b> <!-- 1 --></td>
</tr>
<tr>
<td>Mario Aichinger <!-- 1 --></td>
<td>Markus Brenneis <!-- 1 --></td>
</tr>
<tr>
<td><b>Markus Ebner</b> <!-- 1 --></td>
<td>Nicolás Alvarez <!-- 1 --></td>
</tr>
<tr>
<td>Olivier Goffart <!-- 1 --></td>
<td><b>Pani Ram</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Pat Brown</b> <!-- 1 --></td>
<td><b>Paul Brown</b> <!-- 1 --></td>
</tr>
<tr>
<td>Philipp A <!-- 1 --></td>
<td><b>Roman Hujer</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Tobias Kündig</b> <!-- 1 --></td>
<td><b>Wagner M Cunha</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Wale Afolabi</b> <!-- 1 --></td>
<td>Wes H <!-- 1 --></td>
</tr>
<tr>
<td><b>Yuhang Zhao</b> <!-- 1 --></td>
<td><b>Yunhe Guo</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>hololeap hololeap</b> <!-- 1 --></td>
<td><b>oldherl oh</b> <!-- 1 --></td>
</table>
</p>

I hope to see even more bold marked entries on our teams page end of the next year!
We really need more people that help us to both fix our existing bugs and extend on the solid base of features we have.
Everybody is welcome [to join](/join-us/)!

But already now, to all people contributing: **THANKS A LOT!**

## Great new stuff in the editor in 2021?

Below I picked just two things I heavily use, this is not the full list of cool new stuff we have.
There are that many nice improvements everywhere, like fuzzy filtering in a lot of places, a redone faster search & replace in files,
the new HUD & redone quick open, git blame support, ...., I just wanted to highlight a few things ;=)

If you are more interested in that stuff, just take a look at [the blog posts](/post/) of this year!

Everybody will have other stuff they like.
Some people love the new HUD and will use it the whole day, I use it rarely, mostly to trigger stuff I can't remember where we put that in the menus ;=)
On the other side I use the whole day the project plugin with the search in files plugin and "pgrep somestuffyousearch".

### Improved Project & Git Handling

A lot of improvements went into the project plugin that has now very nifty Git management capabilities, too.
See below me working on this blog post inside Kate ;=)

<p>
<center>
<a href="/post/2021/2021-12-31-kate-in-2021/images/kate-git-project.png"><img width="600" alt="The Kate Mascot" src="/post/2021/2021-12-31-kate-in-2021/images/kate-git-project.png"></a><br />
</center>
</p>

As you can see, one can nicely mix terminal usage Git commands with the cool new graphical overview of the current state & diffing.

Naturally you can add files via the GUI, too ;=) I am just used to type git add :P

I more or less use this daily at work and for my projects, it is really great.
Finally you can easily diff stuff in Kate before commiting, you can write proper commit message there and more.

### Improved LSP Integration

Another thing I use daily is the LSP client plugin.
If LSP doesn't click for you, it is the integration for code completion and more in a similar way like Visual Studio Code or Atom do it ;=)

<p>
<center>
<a href="/post/2021/2021-02-14-kate-valentines-day/images/kate-lsp-tooltip.png"><img width="600" alt="The Kate Website" src="/post/2021/2021-02-14-kate-valentines-day/images/kate-lsp-tooltip.png"></a><br />
</center>
</p>

This plugin went a long way since the initial integration [in release 19.12](/post/2020/2020-01-01-kate-lsp-client-status/)!

## Other stuff that happened in 2021 for the project

### Improved kate-editor.org Website

Our website is now properly hosted on KDE maintained infrastructure and uses the common KDE styling like other KDE related websites.
The current website infrastructure even supports proper translations and many of top level pages should now be available for non-english speakers, too.

You can easily contribute to this page via merge requests, if you like to write some blog post, too, feel free to submit one!

<p>
<center>
<a href="/post/2021/2021-02-14-kate-valentines-day/images/kate-website.png"><img width="600" alt="The Kate Website" src="/post/2021/2021-02-14-kate-valentines-day/images/kate-website.png"></a><br />
</center>
</p>

There were several iterations until we arrived at the current state, thanks a lot (to Carl and naturally all others involved here)!

A big thanks to all the people that help to translate our websites (and tools), too!

I think the current setup of our page via [Hugo](https://gohugo.io/) is really great.
The full content is managed in our Git repository, no extra databases to backup and fiddle with.
All history of over 15 years of blog posts and pages nicely archived.

### New Mascot

I know, this is nothing the normal user will really benefit from, but for our branding, it is nice to have some recognizable modern mascot.
Head over to [the mascot page](/mascot/) to read the full creation history, I actually think it is kind of interesting.

<p>
<center>
<a href="/images/mascot/electrichearts_20210103_kate_normal.png"><img width="600" alt="The Kate Mascot" src="/images/mascot/electrichearts_20210103_kate_normal.png"></a><br />
</center>
</p>

Thanks again to [Tyson Tan](https://tysontan.com/), for both the mascot (old & new) and the current Kate icon!

## New Year Wishes

For new year wishes, I can just copy over most of the text from last year.

Let's hope 2022 will be a better year for the world!

And I hope we will see a further rise in contributors & contributions for Kate and KDE in general.

Let's rock!

And on a personal note, I hope I get more motivated to e.g. update the Kate & Co. Windows versions again, too.
I somehow did slack off on that area, some help with the Windows part of development/testing would be appreciated, too.
Help there is welcome, I even was not able to finish my trivial fix for [the translation packaging](https://invent.kde.org/packaging/craft/-/merge_requests/44) :(

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/rt0bvj/the_kate_text_editor_in_2021/).
