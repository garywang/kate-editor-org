---
title: KDE & Qt Cooperation
author: Christoph Cullmann
date: 2021-03-24T21:40:00+02:00
url: /post/2021/2021-03-24-kde-qt-cooperation/
---

## Qt (6?) is doomed?

In the last days, I read the [Qt Interest mailing list](https://lists.qt-project.org/listinfo/interest) and was amazed about the very verbose discussion there about what is all bad with Qt in general and Qt 6.
As for any mailing list discussion, that is not representative, but if you just read that, the world looks bleak.

A short (personal) digest could read like: Qt 5 => 6 is horrible, the Qt project (and company) doesn't care for their (open source and other) users and the future is doomed for Qt.

## Really?

Now, stepping away from plain discussions on lists or forums, let's look at some real world example for a Qt 5 => Qt 6 transition.

Take a look at [this merge request](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/184) for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/).
It is nice to demonstrate two things.

## The Qt 5 => Qt 6 transition is no disaster

Qt 6 is still lacking many modules people use compared to Qt 5, that is true, but that was well communicated.
On the other side, if your project only uses modules that are already ported, the transition is no plain hell.

Look at the above mentioned [merge request](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/184).

Porting the [KSyntaxHighlighting framework](https://invent.kde.org/frameworks/syntax-highlighting/) to Qt 6 and keeping it compiling with Qt >= 5.14 required 56 additions and 39 deletions in a project of ~7,500 lines of non-trivial code.
And most of these changes port to better API we could have already used before, given it now compiles still with 5.14.
(ok, that was cheating, I needed to commit a one-liner fix afterwards to fix compile with 5.14 and not only 5.15, [see here](https://invent.kde.org/frameworks/syntax-highlighting/-/commit/fb03f52a58be2899e865080747621b94d699db57))

And yes, that is close to everywhere without any strange #ifdef hell.
The only #ifdef stuff is there for trivial things like the no longer needed (as now default) encoding setting for some streams, see one example below:

{{< highlight cmake >}}
QTextStream in(&f);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    in.setCodec("UTF-8");
#endif
{{< /highlight >}}

If one would have ditched the Qt 5 support, the changes would have even be smaller.

Naturally, this only is feasible, as KSyntaxHighlighting doesn't use any deprecated API of Qt 5.15 since long.
But I regard this to be proper code evolution.
If you just disregard any deprecation warnings for years, for sure, the switch will be more painful.
I would not regard KSyntaxHighlighting as a high profile project with a lot of manpower, but keeping up with the gradual API changes was there no issue.

On the other side, if your don't evolve you code anyways for X years, perhaps it is better to just stick with Qt 5, that won't be taken away even with LTS releases being delayed for open source users.

## The Qt developers do care about other projects

First, to clarify, "the Qt developers" is perhaps a bit undefined, I would count all Qt project contributors to this group.
Often, if people start discussions about the in their eyes sorry state of Qt, the Qt developers are the in their view "evil" Qt Company employees.

But, now, if you look at [our nice merge request](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/184), incredible, somebody that works on Qt Creator did really open this.
On his own.
No, we didn't coerce him.
He did say: for Qt Creator we need to have Qt 6 support, here it is, do you like it?
And yes, that was no plain code dump, he got some feedback and adapted it and now we merged it.

This shows, our decision in 2018 to [let KSyntaxHighlighting use the MIT license](/2018/10/21/mit-licensed-ksyntaxhighlighting-usage/) to allow the use in Qt Creator was no one-way ticket.
The Qt Creator developers don't just patch their copy and are happy with their own toy, they come actively back to us and upstream such things.

## The future is all bright now?

Naturally, no, the future still has a lot of challenges.
The KDE project will still need to handle the Qt 5 => Qt 6 transition for Frameworks and the other parts.
That is plenty of code that will need changes, not just for the pure porting but to make it more future proof, too.
And yes, KSyntaxHighlighting is a microscopic part of this big picture.

This weekend there will be a virtual [KDE Frameworks 6 Sprint](https://www.volkerkrause.eu/2021/03/20/kf6-sprint-2021.html) to further proceed with this.
This is open for all interested parties, if you want to know more, [take a look at our wiki](https://community.kde.org/Sprints/KF6/2021Virtual).
