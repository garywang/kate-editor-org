---
title: Git Blame Plugin for Kate 21.04 Take 2
author: Kåre Särs
date: 2021-04-01T16:00:00+03:00
url: /post/2021/2021-04-01-kate-21.04-git-blame-2/
---

## Nifty new feature in Kate for KDE Gear 21.04

So OK the previous blog was a bit exaggerated. There is no AI in the works (right now at least) :) But with [KDE Gear 21.04](https://community.kde.org/Schedules/KDE_Gear_21.04_Schedule) there *will* actually be be a Git Blame plugin.

### So what does it do for real?

Ever wonder why a code line is as it is? Git helps with that. There is the git blame command that provides information about when and who committed a line in a file. Using the commit hash, printed by git blame, it is possible to read the commit message and diff. That is usually a good place to start finding the clues for why it was written in that strange way.

What the git blame plugin does is, it tries to run git blame on the current file and when we are in a git repository it uses the nifty InlineNoteProvider interface of KTextEditor to display the author, date and title of the commit message on the current line.

![Inline Note](/post/2021/2021-04-01-kate-21.04-git-blame-2/images/git-blame-inline-comment.png)

If you click on the inline-note it will open up a popup window with the whole git log of the commit, including the diff.

![Popup window](/post/2021/2021-04-01-kate-21.04-git-blame-2/images/git-blame-popup.png)

If you are on a very long line you might not be able to click or see the inline-note. In this situation you can open the popup with it's shortcut. The default shortcut for opening the blame information popup is Ctrl+Alt+G

Hovering over the popup keeps it alive and you can copy the content of it if needed.

#### Try it out!

If you still are interested and can't wait for the KDE Gear 21.04 release and distro-packaging (and you did not follow the link the first time), install one of the nightly packages at [Get Kate](https://kate-editor.org/get-it/) And this does work on Windows too (I have not tested on Mac, but it should work) and then go to Settings -> Configure Kate... -> Plugins and enable the git-blame plugin.

Thanks for your attention and hope the plugin will be of use :)

<p align="center">
    <img width=256 src="/images/kate.svg">
</p>
