---
title: KDE e.V. Windows Store Statistics
author: Christoph Cullmann
date: 2021-12-05T17:10:00+03:00
url: /post/2021/2021-12-05-kde-ev-windows-store-statistics/
---

Soon the year ends, let's take a look at the current statistics of all applications in the Windows Store published with the KDE e.V. account.

Since the last summary in [June](/post/2021/2021-06-19-kde-ev-windows-store-statistics/), [KDE Connect](https://kdeconnect.kde.org/) joined as newly available application!
Thanks to all people that made this happen.

### Last 30 days statistics

Here are the number of acquisitions for the last 30 days (roughly equal to the number of installations, not mere downloads) for our applications:

* [KDE Connect - Enabling communication between all your devices](https://www.microsoft.com/store/apps/9N93MRMSXBF0) - 6,368 acquisitions

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 6,197 acquisitions

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 3,435 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 836 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 523 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 239 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 199 acquisitions

* [LabPlot - Scientific plotting and data analysis](https://www.microsoft.com/store/apps/9NGXFC68925L) - 143 acquisitions

A nice stream of new users for our software on the Windows platform.

One issue we have here is: Kate/Okular/... are still release 21.04.
It would be cool if people could step up and help to update the releases there.
Actually, on just needs to grab a version of the binary factory and test it and propose it as update, that would already help.

### Overall statistics

The overall acquisitions since the applications are in the store:

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 137,378 acquisitions

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 121,460 acquisitions

* [KDE Connect - Enabling communication between all your devices](https://www.microsoft.com/store/apps/9N93MRMSXBF0) - 36,226 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 31,763 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 16,178 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 8,025 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 6,983 acquisitions

* [LabPlot - Scientific plotting and data analysis](https://www.microsoft.com/store/apps/9NGXFC68925L) - 1,581 acquisitions

### Help us!

If you want to help to bring more stuff KDE develops on Windows, please contact the maintainers of the stuff you want to help out with.

A guide how to submit stuff later can be found [on our blog](/post/2019/2019-11-03-windows-store-submission-guide/).

Thanks to all the people that help out with submissions & updates & fixes!

If you encounter issues on Windows and are a developer that wants to help out, all KDE projects really appreciate patches for Windows related issues.

Just contact the developer team of the corresponding application and help us to make the experience better on any operating system.
