---
title: KDE Framework 5.84 - Expandable Tooltips
author: Christoph Cullmann
date: 2021-06-13T20:26:00+03:00
url: /post/2021/2021-06-13-kde-frameworks-5.84-expandable-tooltips/
---

Starting with the KDE Frameworks 5.84 release, [KXMLGUI](https://invent.kde.org/frameworks/kxmlgui) based applications will feature expandable tooltips per default.

The matching [merge request by Felix Ernst](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/45) got merged today after 3 months ;=)

## What are expandable tooltips at all?

Good question ;=)

In short: for stuff like menu & toolbar actions, it provides an easy way to access the "What's This?" help.

Unlike before, where you need to manually trigger that via the "Shift-F1" shortcut and click around to try out which GUI elements provide at all this additional help, you will now first get a small tooltip with the normal tooltip content (if any) and a hint that with "Shift" you are able to get more help displayed.

For more details read the [merge request](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/45).

## A video is better than words ;=)

Felix provided a short video in the merge request to demonstrate the basic usage (based on the first implementation, some minor details got fine-tuned later).

<video width=100% controls>
<source src="/post/2021/2021-06-13-kde-frameworks-5.84-expandable-tooltips/videos/2021-04-19_23-01-38.mp4" type="video/mp4">
Your browser does not support the video tag :P</video>

## Why is that great?

With this feature the "What's This?" help is a lot easier to discover.
It actually makes now more sense then ever to provide it for more complex actions in your applications, people will really be able to find it.

And even nicer, as this is now done in KXMLGUI, all applications using this KDE Framework will automatically provide the new feature, like e.g. Kate ;=)

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/nz2ncy/kde_framework_584_expandable_tooltips/).
