---
title: Git Blame Plugin for Kate 21.04
author: Kåre Särs
date: 2021-04-01T01:00:00+03:00
url: /post/2021/2021-04-01-kate-21.04-git-blame/
---

## Nifty new feature in Kate for KDE Gear 21.04

With [KDE Gear 21.04](https://community.kde.org/Schedules/KDE_Gear_21.04_Schedule) there will be a new plugin available for Kate: Git Blame.

### So what does it do?

Every body knows it is a lot of work to go through bug reports and fix the bugs. As we all know lazy developers (that is the good type) do not want to do tedious work and try to automate boring tasks. This is where the git blame plugin comes in.

#### Crash Fixes

Feed the plugin a crash-dump of an application and provide the git repository of the application sources. The plugin will then, using AI, go process the crash-dump and locate the crash location and the reason for the crash. The plugin can pinpoint the commit (and committer) that introduced the crash. Hens, git blame ;) The plugin can also suggest a fix for the crash. The results where actually surprisingly good. In around 70% of our test cases, the algorithm could generate a good working fix for the crash. The rest needed minor modifications to make them work properly.

![Automatic crash fix](/post/2021/2021-04-01-kate-21.04-git-blame/images/auto-fix-crash.png)

#### How did we do it?

We scraped bugs.kde.org for crash reports for Kate. We took the crash reports that also included a fix commit for the crash.
*Note*: This would not have been possible without the BUG keyword. So please remember to always use these keywords when possible.

We fed the machine-learning algorithms with these crash reports and the fixes that have been done for Kate, during Kates more than 20 years long history.


#### Regression Fixes

Feed the git-blame plugin a regression-bug report and a rough time when it was introduced and the plugin will again use AI to figure out the git-commit that introduced the regression and in some cases even provide a fix for the regression. In this case the fix accuracy was much smaller in our tests. Only about 30% provided accurate fixes, but the pinpointing of the git-commit is really helpful.


#### How did we do it?

This feature required quite a lot more machine-learning. We now used multiple KDE Gear (formerly Applications) git repositories and Bugzilla bug-reports for getting enough samples.


#### Basic Feature Requests

Feed the plugin a basic feature request, like "use text selection highlight as kate minimap highlight color", and it will in most cases find the correct location to modify and in some cases even do the minor feature. This feature was actually used to implement just that request :) With Kate 21.04 (KTextEditor 5.81) the scroll-bar minimap highlight color is the text highlight color in stead of web-link color.

#### How did we do it?

With the positive results from the regression fixes we, expanded the scraping to all of KDE.
In the beginning we used all the wish-list reports, but after some time of failures we hand selected some of the best mini-wish-list reports and fed them to the algorithm. This provided some improvements and now we have a success rate of around 15% for minor features.

#### Try it out!

If you are interested and can't wait for the KDE Gear 21.04 release and distro-packaging, install the [Nightly  (64bit) AppImage](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) (unfortunately this is a Linux only feature so far no Windows or Mac version yet) and then go to Settings -> Configure Kate... -> Plugins and enable the git-blame plugin.

Thanks for your attention and hope the plugin will be of use :)

<p align="center">
    <img width=256 src="/images/kate.svg">
</p>
