---
title: The Kate Text Editor - Valentine's Day 2021
author: Christoph Cullmann
date: 2021-02-14T20:33:00+02:00
url: /post/2021/2021-02-14-kate-valentines-day/
---

Kate, KTextEditor and Co. did get a nice stream of updates in the first two weeks of February 2021.

I will just pick a few things I really liked, if you want to have a full overview, you can go through the list of [all merged patches](/merge-requests/).

# Even more multi-threading in search in files

After the initial parallelization of the actual search in files as described [here](/post/2021/2021-01-29-search-in-files-multi-threading/), Kåre came up with the idea to parallelize the creation of the file list we use for the search, too.

We worked together on this in [merge 220](https://invent.kde.org/utilities/kate/-/merge_requests/220) and [merge 221](https://invent.kde.org/utilities/kate/-/merge_requests/221), the result will allow for even faster searches if no initial file list is provided already by e.g. the project plugin.

I didn't actually believe that would be worth the hassle, but Kåre provided impressive speedups in [merge 220](https://invent.kde.org/utilities/kate/-/merge_requests/220#note_180083), from over 30 seconds down to around 3 seconds, nice!

# Improvements to the color picker plugin

The new color picker plugin got some [further improvements by Jan Paul](https://invent.kde.org/utilities/kate/-/merge_requests/190), if you missed what it does, see screenshot below, it shows the color for #hexcodes inline in the text editor view + provides a color picker to alter it.

![Screenshot of Kate color picker](/post/2021/2021-02-14-kate-valentines-day/images/kate-color-picker.png)

# Open a project per command line

So far, Kate projects can only be opened indirectly by opening a file inside the project or launching Kate from a terminal inside some directory of a project.

Alexander rectified this now, Kate will allow to open a project just by passing the directory as argument after [this merge](https://invent.kde.org/utilities/kate/-/merge_requests/219).

This sounds like a rather trivial change, but I guess it will make working with projects more natural.

# Allow to switch branches in Git

Waqar works on better Git integration.
This has been an idea since years but we never got around to it ;=)
Finally we see some progress.

![Screenshot of Kate git branch selector](/post/2021/2021-02-14-kate-valentines-day/images/kate-git-checkout.png)

The current implementation inside the project plugin shows prominently the current branch of your clone below the project name.
Pressing this button allows you to either switch to an existing branch or create a new one and switch to it.
As usual, this quick open like dialog provides the fuzzy matching we now use close to everywhere in Kate.

# Improving LSP contextual help

Waqar worked on better visualization of the contextual help via LSP, too.
So far, we just used some normal tooltips, unfortunately that doesn't work that well for the rich content we get from typical LSP servers like clangd.
Now we have some custom widget for this with proper highlighting and font/color matching the editor theme.

![LSP tooltip](/post/2021/2021-02-14-kate-valentines-day/images/kate-lsp-tooltip.png)

# Improved website theme

Perhaps this change is obvious, as already visible if you read this post on our website [kate-editor.org](https://kate-editor.org/), but we now use the shared theming other KDE websites use, too.
If you read this from some aggregator, take a look at the start page screenshot below.

![Screenshot of Kate website](/post/2021/2021-02-14-kate-valentines-day/images/kate-website.png)

Carl [worked on this](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/18), he did a fantastic job.

There were several iterations until we arrived at the current state, thanks a lot (to Carl and naturally all others involved here)!

A big thanks to all the people that help to translate our websites (and tools), too!

## Help wanted!

You want more nifty stuff? More speed? More of everything?

You want to make our website even nicer?

Show up and [contribute](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/).

We have [a lot of ideas](https://invent.kde.org/utilities/kate/-/issues/20) but not that many people working on them :)

# Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/ljvhti/the_kate_text_editor_valentines_day_2021/).
