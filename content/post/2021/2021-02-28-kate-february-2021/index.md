---
title: The Kate Text Editor - February 2021
author: Christoph Cullmann
date: 2021-02-28T16:12:00+02:00
url: /post/2021/2021-02-28-kate-february-2021/
---

Like in [January 2021](/post/2021/2021-01-24-kate-january-2021/), a lot of stuff happened for Kate in February 2021, too.
I will skip the stuff that I already reported on [Valentine's Day](/post/2021/2021-02-14-kate-valentines-day/).
The web site for example has still a new design and [some people are still working behind the scenes](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged) to improve it!

Let's take a look at which cool new stuff you can expect to have in the 21.04 release of Kate.
If you are adventurous: [build the current development version yourself](/build-it/) and try the stuff today.
Feedback & [patches](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/) welcome!

# Improved Git Integration

In the [Valentine's Day post](/post/2021/2021-02-14-kate-valentines-day/) I already mentioned that you now can now switch the branch from inside Kate as can be seen below.

![Screenshot of Kate Git branch selector](/post/2021/2021-02-14-kate-valentines-day/images/kate-git-checkout.png)

After more work by Waqar our Git integration can now do a lot more, see below a small gallery of stuff that is already possible.
This is not all stuff you can do with it, just some coarse overview ;=)

## Initial Staging / Diff Handling

![Screenshot of Kate Git diff/staging context menu](/post/2021/2021-02-28-kate-february-2021/images/kate-git-stage.png)

## Create a Commit

![Screenshot of Kate Git commit dialog](/post/2021/2021-02-28-kate-february-2021/images/kate-git-commit.png)

## Simple Stash Handling

![Screenshot of Kate Git stash context menu](/post/2021/2021-02-28-kate-february-2021/images/kate-git-stash.png)

## Outlook

For sure, important things are still missing, like pull/push support, but at least on Unices, one can use the integrated terminal for this at the moment, like before.
We will try to get the remaining basics done for 21.04.

We want to have some dedicated Git workflow icons, too.
See [this issue](https://invent.kde.org/frameworks/breeze-icons/-/issues/2) for progress on that.
Thanks to Dimitris on helping to create Breeze icons for this, not only Kate will profit from that!
In this issue is actual proof that Dominik can use pen & paper, unlike me ;=)

![Photo of proposed Git workflow icons](/post/2021/2021-02-28-kate-february-2021/images/git-icons.jpg)

## Words of Caution

Don't expect Kate to be some full-featured Git GUI with all stuff you can do with Git.
You can still launch any external GUI for this or use the integrated terminal.
The main focus of Kate's Git integration is to help you to work with your project in a light-weight way to avoid the use of the terminal for common tasks.

# More Static Checking in KSyntaxHighlighting

This is not directly user visible, but helps to keep the quality of our default syntax definition XML files high.

Jonathan works a lot on improving the initial analysis we do on these files during compilation.

Already a long time ago, these checks got mandatory, that means: no, if you e.g. reference a non-existing attribute in your XML file in our repository, KSyntaxHighlighting will not build.
It is no test error you can ignore, this is just a hard error like any compiler error.

In the last weeks, Jonathan added more and more checks, like [detection of unreachable rules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/160) (naturally, this is not exhaustive, but catches already many cases).
Alone in this one request you can see how this helped to cleanup the existing definition files.

If you are interested in other related changes, browse over [this list](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged).

# Bugfixes for KTextEditor

KTextEditor, serving as the text widget used by multiple applications beside Kate, got a bunch of bug fixes.

A lot of them were done by Jan Paul, thanks for taking care of this stuff!
I assume VI mode users will be happy to hear that some of their issues got resolved.

He even works on tedious stuff like trying to let the [search & replace stuff behave better](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/94).

![Animation of KTextEditor search bar](/post/2021/2021-02-28-kate-february-2021/images/kate-search-unsaved-text.gif)

Beside this, a lot of refactoring happened in the last weeks, too.
Thanks to Ahmad for taking care of moving to the modern style connect syntax for example and other cleanups!
Unfortunately such work is tedious and error prone.
We did introduce some minor regressions, I hope most of this is fixed in the upcoming 5.80 Frameworks release.
And to make it clear, this is no fault of the person doing the changes, it wasn't noticed in the review and in the end it comes down to having too few tests.

Therefore, if you want to really do major non-trivial refactoring, like let's say "I want to redo the search & replace the right way", we will absolutely require that we get a lot more unit tests for that first.
Even trivial, mostly syntactic changes can introduce faults, large scale refactoring even more.

# Finally: A Generic Output View in Kate

For years we just always thought "would be great", but now, that we have more plugins that want to output stuff, it was time to create it.

The initial variant landed [here](https://invent.kde.org/utilities/kate/-/merge_requests/296).
Since then, several more extensions were done.

![Screenshot of Kate output view](/post/2021/2021-02-28-kate-february-2021/images/kate-output.png)

Any plugin can emit messages and the Kate application will then collect them in this extra output tool view per main window.
If a plugin emits stuff, this will land in all main windows, if the plugin view (there is one per main window) emits a message, it will only land in the corresponding window output.

At the moment, already the Git integration, LSP & external tools plugin use this mechanism.
After we fledge out a "stable" message format, we will document this properly in the KTextEditor interfaces to allow other host applications to implement the same mechanism if wanted.
The current documentation can be found [here in the Doxygen comment of the slotMessage](https://invent.kde.org/utilities/kate/-/blob/master/kate/kateoutputview.h#L40).

Btw., a big praise to our documentation people, even thought I am not able to use the proper key words in my commits, [Yuri already updated the documentation for this](https://invent.kde.org/utilities/kate/-/commit/aac6b0fc03571d123b2f27320d109b6eeadcff8c)!
Awesome, you rock! :=)

# Help wanted!

Show up and [contribute](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/).

We have [a lot of ideas](https://invent.kde.org/utilities/kate/-/issues/20) but not that many people working on them :)

# Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/luid33/the_kate_text_editor_february_2021/).
