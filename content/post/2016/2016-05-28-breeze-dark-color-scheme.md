---
title: Breeze Dark Color Scheme
author: Dominik Haumann

date: 2016-05-28T12:14:29+00:00
url: /2016/05/28/breeze-dark-color-scheme/
categories:
  - Developers
  - Users

---
Just as quick info, with the next KDE Frameworks 5 release, namely KF5 version 5.23, the KTextEditor framework gains a <a href="https://cgit.kde.org/ktexteditor.git/commit/?id=fc45b9461c505311586c455ebe85e53b2eb6c901" target="_blank">Breeze Dark color scheme</a>. The colors mostly stick to the Breeze color palette, with some minor changes, since KTextEditor needs more colors the the color palette itself ships. To use this color scheme, go to the config dialog and choose &#8220;Breeze Dark&#8221; in the Fonts & Colors config page. We hope this is useful &#8211; mandatory screenshot:

[<img class="aligncenter size-full wp-image-3747" src="/wp-content/uploads/2016/05/breeze-dark.png" alt="Breeze Dark" width="1920" height="1027" srcset="/wp-content/uploads/2016/05/breeze-dark.png 1920w, /wp-content/uploads/2016/05/breeze-dark-300x160.png 300w, /wp-content/uploads/2016/05/breeze-dark-1024x548.png 1024w" sizes="(max-width: 1920px) 100vw, 1920px" />][1]

**Update:** Screenshot of KDevelop 5 with Breeze Dark theme and color schema:

[<img class="aligncenter size-full wp-image-3754" src="/wp-content/uploads/2016/05/kdevelop5-breeze-dark.png" alt="KDevelop 5 with Breeze Dark" width="1273" height="944" srcset="/wp-content/uploads/2016/05/kdevelop5-breeze-dark.png 1273w, /wp-content/uploads/2016/05/kdevelop5-breeze-dark-300x222.png 300w, /wp-content/uploads/2016/05/kdevelop5-breeze-dark-1024x759.png 1024w" sizes="(max-width: 1273px) 100vw, 1273px" />][2]

 [1]: /wp-content/uploads/2016/05/breeze-dark.png
 [2]: /wp-content/uploads/2016/05/kdevelop5-breeze-dark.png