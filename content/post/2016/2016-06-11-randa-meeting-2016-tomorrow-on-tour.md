---
title: Randa Meeting 2016 – Tomorrow on Tour ;=)
author: Christoph Cullmann

date: 2016-06-11T11:31:02+00:00
url: /2016/06/11/randa-meeting-2016-tomorrow-on-tour/
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
Tomorrow I will start to travel to the [Randa Meeting][1] 2016.

I hope I can help to make it possible to spread KF5 based stuff more easy on platforms like Windows & Mac.

Lets see what can be done, will be an interesting week ;=)

If you want to support us, please consider to donate, we have a [fundraising campaign][2] running!

[<img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/fundraising2016-ogimage.png" width="1200" height="627" />][2]

 [1]: http://randa-meetings.ch/
 [2]: https://www.kde.org/fundraisers/randameetings2016/