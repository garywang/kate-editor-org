---
title: 'Kate Modes & Filetypes'
author: Christoph Cullmann

date: 2006-02-09T15:57:45+00:00
url: /2006/02/09/kate-modes-filetypes/
categories:
  - Common

---
Modes allow the user to override the default configuration for documents of specified mimetypes or file extensions. When the editor loads a document and the mimetype or file extensions match for one of the defined filetypes, KatePart will read the configuration from the mode and apply the modelines defined. If more filetypes match, the one with the highest priority will be used.

To create a filetype, go into the settings dialog available in Kate via _Settings > Configure Kate > Open/Save > Modes/Filetypes_, then press the _New_ button and fill in the properties. To remove an existing filetype, select it from the dropdown list and press the _Delete_ button. To edit an existing mode, select it from the dropdown list.

### Some Available Properties

  * **Name**  
    This is a user friendly name that will be displayed in the menu _Tools > Mode_.
  * **Section**  
    A submenu of _Tools > Mode_ in which to place this filetype. If the section does not already exist, it will automatically be created.
  * **Variables**  
    This is where the actual configuration goes. Variables set here will overwrite the default configuration values. Be aware that this line must begin with **kate:** and end with a semicolon. For a full list of available variables refer to <a title="Kate Modelines" href="/2006/02/09/kate-modelines/" target="_self">modelines</a>.
  * **File Extensions**  
    A semicolon separated list of filename globs used to determine if a document matches this filetype.
  * **Mimetypes**  
    A semicolon separated list of mime types used to determine if a document matches this filetype.
  * **Priority**  
    If a document matches several filetypes, the one with the highest priority will be chosen.

### Example

As an example we will create a new mode configuration for C++ and Java files. It should use the C Style indenter and indent with 4 spaces (no tabs). The configuration could look like this:

    Name: C++ and Java settings
    Section: Sources
    Variables: kate: space-indent on; indent-width 4; mixed-indent off; indent-mode cstyle;
    File Extensions: *.cpp;*.java
    Mimetypes: text/x-c++hdr;text/x-c++src;text/x-java
    Priority: 10