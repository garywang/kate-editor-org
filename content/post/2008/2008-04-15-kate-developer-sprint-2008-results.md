---
title: Kate Developer Sprint 2008 Results
author: Christoph Cullmann

date: 2008-04-15T11:58:22+00:00
url: /2008/04/15/kate-developer-sprint-2008-results/
categories:
  - Events

---
The below notes is what was decided at the development sprint in Darmstadt on April 11-13, 2008

## Short term goals

  1. Scripting, part level QtScript
  2. API for indentation scripts
  3. Sessions handling
  4. make text completion work!
  5. VI-Modes (GSoC-project)

## Long term goals

  1. Scripting at application level (Kate): Kross
  2. Combination of highlightings & indentations

## Table of Contents

  * Scripting
  * Indentation
  * Kate Sessions
  * Extending the highlighting system (Highlighting combination)
  * Collaborative editing
  * Text input modes (vi mode)
  * Minor topics: Search & Replace, text completion
  * Interface Review
  * Simplifying KWrite

## Scripting

  * Use cases 
      * indentation
      * helper scripts + assign shortcut
      * useful for vi-mode scripting
  * right now: kjs (barely documented, future uncertain)
  * choices: Kross, QtScript 
      * Kross: language independent, depends on available bindings/plugins at runtime
      * QtScript, Kross: simply wrap QObjects (signals/slots, Q_SCRIPTABLE automatically exported)

### Kate Part: QtScript

  * flexible integration possible
  * make it fast for specific use cases (e.g. indentation)
  * script header 
      * meta information: key/value pairs (type, name, mimetype, wildcard, &#8230;)
      * contact i18n people: how to do translation
      * translation possible outside KDE?

### Kate App

  * Kross

## Indentation

  * allow multiple indenters for documents with mixed languages (e.g. php and html) 
      * document variables: e.g. indent-mode <language> <mode> [<language> <mode>];
      * document variables: allow different settings (indent-width, &#8230;)
  * remove option &#8220;Default Indenter&#8221;, it does not make sense (really?)
  * the script header should state which hl-modes it can indent (instead of mimetype/wildcard in the header)

## Auto-completion:

  * build word completion plugin into kate part by default
  * fix implementation to actually complete stuff
  * simplfy the popupmenu (!), just as in KDE3
  * remove code completion config dialog, this must be implemented in the code completion model, as this is not applicable to any other than the cpp model. Better have something tiny working instead of a broken monster

## Kate Sessions (profiles)

  * data stored: 
      * application plugin config
      * file list (opened files)
      * window configuration
      * editor component config
      * ktexteditor plugin config (maybe kpluginselector problems)
  * session level configuration / document variables
  * kill default.katesession file, always use katerc
  * do not show the session chooser at startup, instead, make a good default (katerc)
  * clone session: &#8220;save as&#8221; opens a small dialog with line edit and options like window config, file list
  * remove option &#8220;restore view config&#8221; -> simply always do it
  * make sure kate part plugins can save data to sessions: fork kpluginselector, if necessary :)

## Highlighting

  * extend highlighting system to allow combined highlighting 
      * add e.g. &#8220;entercontext&#8221; rules
      * separate highlighting loader from hightlight class

Explanation: The purpose of this is to combine template languages such as PHP, EmbPerl, Mason etc with _any_ other syntax highlighting _on the fly_, as opposed to the current practice which requires a combined set of files to be generated at build time. This is only done for the HTML + PHP combination, so with the new way a multitude of combination becomes available. In addition to that, nothing needs to be regenerated when a highlight is updated, makeing it much easier for users to keep their highlightings updated.

## Highlighting / Modes

  * when opening the config page choose the current used entries (highlight and mode) in the combo box, so that it is immediately clear to the user which mode/hl is active 
      * &#8220;Fonts & Colors&#8221; > Highlight Text Styles
      * &#8220;Open/Save&#8221; > Modes & Filetypes
      * -> implement in kate part internally: simply track an active view (the last that got focus)

## Collaborative Editing

  * example: gobby
  * as KTextEditor plugin? 
      * probably not completely possible right now, missing internal access
  * Decibel (realtime communication protocol) 
      * move communication framework into separate process and use dbus interfaces
  * implementation postponed

## Input modes (vi mode)

  * background: yzis 
      * rewrite from scratch
      * idea of abstracting away &#8220;everything&#8221; to be flexible (e.g. to support different front ends)
      * development stalled 2 years ago
  * right now in kate part 
      * command line already similar to vim&#8217;s &#8220;input mode&#8221;
      * keep this mode + extend, if needed
  * idea: integrate into kate part: extend/refactor kate to support input modes 
      * make code reusable (in other modes)
      * extend KTE::Commands to support ranges (e.g. <1,10>)
      * add &#8220;normal mode&#8221;

## Search & Replace

  * bar has big minimum width, works bad in split view
  * make it possible to embed widgets in the host application (KTE extension?)
  * search & replace over multiple buffers

## Interfaces

  * Rule of thumb: Introduce interfaces only if usecase is known and best thing is always to have an implementation ready to get out the bugs before binary compatibility issues count

### Annotation Interfaces

  * derive AnnotationViewInterface from AnnotationInterface
  * &#8212;> allow for KTE::Document and KTE::View
  * merge header files into one

### Spellchecking Interface / Viewport Interface

  * both aim to support Kile doing spellcheck
  * conclusions 
      * spellcheck should be first integrated in katepart
      * on-the-fly spellcheck support
      * highlighting should feed info which parts are able to be spellchecked (extend highlighting system)
      * after this is done, an interface can be invented to help Kile to enrich the spellcheck with meta-info like languages, additional spellcheckable ranges, &#8230;

### Message Interface

  * implement a way to show messages in a non-intrusive way in the view bar
  * avoid message boxes by using it, this won&#8217;t influence the workflow!
  * allow quering the user (buttons like OK, Yes, No, i.e. KMessageBox-like)
  * multiple messages should be queued (or stacked?)
  * allow both implementation in the part for messages per view and a fallback provided by the application for messages when no view is available

### Load/Save Filter Interfaces

  * preprocessing/postprocessing of text streamed from/to file
  * checking of input/output, warning, error, &#8230;
  * allow lazy loading, using .desktop files to describe mime-type + wildcards
  * allow to trigger usage of the plugins via document vars or mode config
  * default: not allow any plugin, user must allow them (!)

## KWrite: should be more simple

  * extend the kate part to allow blacklist/whitelist highlightings/plugins
  * remove some advanced actions from ui file, provide simple mode, dummy mode
  * remove hl
  * remove folding option
  * remove bookmarks (menu, settings)
  * remove modus
  * remove almost all of extras
  * all in all: strip down to make it usable for the average user