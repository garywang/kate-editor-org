---
title: A Flashback of Kate in Gitorious
author: Dominik Haumann

date: 2010-06-24T20:32:00+00:00
url: /2010/06/24/a-flashback-of-kate-in-gitorious/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/06/flashback-of-kate-in-gitorious.html
categories:
  - Developers

---
Back in February, I blogged about <a href="/?p=109" target="_self">Kate&#8217;s move to gitorious</a>. The main reason for this move was to make building Kate as easy as possible. If you want to build Kate as part of KDE, (as of now) you have to compile kdesupport, phonon, dbusmenu-qt, kdelibs, kdepimlibs, kdebase for kwrite and kdesdk for the kate application. Getting all this done is a huge effort, especially if you are new to KDE development (I very well remember my own times spending weeks to get everything going. Be aware of new contributors might now close to nothing about KDE and all the dependencies!).  
As <span style="font-style: italic;">getting new contributors is essential for keeping a project alive</span>, the barrier to get involved should be as low as possible. And exactly this was achieved by moving all pieces to one place (this was gitorious for us). <a title="Building Kate" href="/get-it/" target="_self">Building Kate is so simple right</a> now that we can even make bug reporters build Kate out of the box. This helps a lot, and even results in patches from time to time. We also got quite some merge requests.  
There were several voices at that time that considered moving &#8220;away from KDE&#8221; was very bad. However, this is not the case, as Christoph is synchronizing all the changes in KDE&#8217;s subversion and gitorious almost every day. This is certainly not optimal, but looking back at the last months, we can say it was worth it.  
KDE is moving to [git.kde.org][1] in the near future. This also raises the discussion about how KDE&#8217;s source code will be organized. Speaking for Kate, we certainly want to have all of Kate&#8217;s code in one place, just as it is now with gitorious, no matter what :) I hope we can find a solution the KDE community can live with. To be discussed, maybe [in Tampere in two weeks][2]? :)

 [1]: http://cgit.kde.org/
 [2]: http://akademy2010.kde.org/