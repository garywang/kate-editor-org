---
title: Kate Partly Moving to Gitorious
author: Dominik Haumann

date: 2010-02-27T16:55:00+00:00
url: /2010/02/27/kate-partly-moving-to-gitorious/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/02/kate-partly-moving-to-gitorious.html
categories:
  - Developers

---
We are about to move the applications Kate and KWrite as well as the libraries KTextEditor and Kate Part to [gitorious][1]. Christoph is working on the migration right now in order to keep the development history. Things look good so far, so the migration is soon finished.  
We have discussed a bit about the migration to gitorious on the Kate Developer Meeting and Christoph came up with this mainly because building only KTextEditor, Kate Part, KWrite and Kate is much <span style="font-style: italic;">faster and easier</span> compared to building the KDE modules kdesupport, kdelibs, kdepimlibs, kdebase, kdesdk.  
I myself remember the time where I started KDE development, and it took more than two weeks to have a first successful build of KDE. You have to learn so many things at once, like revision control, lots of so far unknown software, and what not. Talking to other developers verifies this. In other words: Getting into KDE development is not easy and straight forward.  
Moving to gitorious removes this barrier for Kate development: You just checkout the Kate repository and that&#8217;s all you need. It would be nice if you join Kate development and contribute patches :)  
<span style="font-weight: bold;">What does that mean for Kate in KDE? <span style="font-style: italic;">Nothing changes.</span> We will merge the changes in Gitorious back to the main KDE development line and vice versa.</span>

 [1]: http://gitorious.org/kate