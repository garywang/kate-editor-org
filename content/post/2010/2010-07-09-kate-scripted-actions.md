---
title: 'Kate: Scripted Actions'
author: Dominik Haumann

date: 2010-07-09T09:04:00+00:00
url: /2010/07/09/kate-scripted-actions/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/07/kate-scripted-actions.html
categories:
  - Developers

---
Finally, I came around to implement scripted actions for Kate in KDE SC 4.6. Let&#8217;s take a look at how this works. When Kate starts, it searches for $KDEDIRS/share/apps/katepart/script/ for *.js files. As example, let&#8217;s take a look at utils.js there:

<pre>/* kate-script
 * author: Dominik Haumann Haumann
 * license: LGPL
 * revision: 3
 * kate-version: 3.4
 * type: commands
 * functions: moveLinesDown
 */

function moveLinesDown()
{
  var fromLine = -1;
  var toLine = -1;

  var selectionRange = view.selection();
  if (selectionRange.isValid() &&
      selectionRange.end.line &lt; document.lines() - 1)
  {
    toLine = selectionRange.start.line;
    fromLine = selectionRange.end.line + 1;
  } else if (view.cursorPosition().line &lt; document.lines() - 1) {
    toLine = view.cursorPosition().line;
    fromLine = toLine + 1;
  }

  if (fromLine != -1 && toLine != -1) {
    var text = document.line(fromLine);

    document.editBegin();
    document.removeLine(fromLine);
    document.insertLine(toLine, text);
    document.editEnd();
  }
}

function action(cmd)
{
  var a = new Object();
  if (cmd == "moveLinesDown") {
    a.text = i18n("Move Lines Down");
    a.icon = "";
    a.category = "";
    a.interactive = false;
    a.shortcut = "";
  }

  return a;
}

function help(cmd)
{
  if (cmd == "moveLinesDown") {
    return i18n("Move selected lines down.");
  }
}</pre>

What happens is the following:

  1. the header tells kate that there is an exported function &#8220;moveLinesDown&#8221;
  2. so when Kate Part is loaded, it calls &#8220;action(moveLinesDown)&#8221; to check whether this function should be exposed in the GUI. Here, we return the action info that includes the displayed text, an icon, a category, whether the script needs user input (interactive) and a default shortcut. Of course, you can change the shortcuts, and also configure the toolbars to show the actions.

With this, every user is able to script arbitrary editing functions for Kate Part. We don&#8217;t have to implement all those helpers in C++ anymore. The result looks like this:  
<a href="http://3.bp.blogspot.com/_JcjnuQSFzjw/TDbqo9NbCQI/AAAAAAAAAEM/8RVBJ2l17mc/s1600/scriptaction.png" onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}"><img id="BLOGGER_PHOTO_ID_5491834784971426050" style="margin: 0px auto 10px; display: block; text-align: center; cursor: pointer; width: 400px; height: 298px;" src="http://3.bp.blogspot.com/_JcjnuQSFzjw/TDbqo9NbCQI/AAAAAAAAAEM/8RVBJ2l17mc/s400/scriptaction.png" border="0" alt="" /></a>You can have this already now, you just have to use the <a title="Building Kate" href="/get-it/" target="_self">development version</a> of Kate :)