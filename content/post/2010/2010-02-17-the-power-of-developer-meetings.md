---
title: The Power of Developer Meetings
author: Dominik Haumann

date: 2010-02-17T14:15:00+00:00
url: /2010/02/17/the-power-of-developer-meetings/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/02/power-of-developer-meetings.html
categories:
  - Events

---
Since several years, we have lots of small [developer meetings in the KDE project][1], gratefully supported by the KDE e.V. There, developers of a certain project (e.g. KMail/kdepim, Plasma, you name it) meet to discuss further plans and to push the project further. From experience we can tell that those meetings are really beneficial in several ways:

  * Social aspect: You get to know the other developers involved in the project in real life, which is a great motivation factor. This also happens at KDE&#8217;s annual conference [Akademy][2], although there are <span style="font-style: italic;">a lot</span> more people.
  * Productivity: Since you are sitting next to each other discussions about <span style="font-style: italic;">how</span> to do <span style="font-style: italic;">what</span> are very focused. It&#8217;s amazing how quickly a project can evolve this way. (I haven&#8217;t seen such focused work in companies, yet. I guess the variance in the knowledge of people involved is higher. And the motivation is usually very different).
  * Knowledge Transfer: Since participants are experts in different areas, discussions lead to knowledge transfer. This is essential, as sometimes developers have very few free time to contributes to a project. Spreading the knowledge helps a lot to keep the project alive.
  * Steady Contributions: We are always open for new contributors. Just send a patch, [get commit access][3] and join development. Experience shows that participants of a developer meeting usually contribute for years to come.

Enough said, here is what happened the last three days in Kate:

  * [memory optimization:][4] [remove unused][5] [edit history items][6]
  * [fix crash in doHighlight due to wrong initialization][7]
  * [don&#8217;t squeeze captions in the window title][8]
  * [fix crash when using katepart in e.g. konqueror][9]
  * [kate vi mode improvements][10]
  * [fix: invalid js commands lead to a crash][11]
  * [share code snippets between kate and kdevelop][12]
  * [more syntax highlighting files for asm dialects][13]
  * [replace: show number of replacements in a passive popup][14]
  * [new command line command: unwrap][15]
  * [fix crash when switching sessions with multiple main windows][16]
  * [fix incorrect global replace %s///g][17]
  * [fix crash in smart range group handling][18]
  * [turn search option &#8220;highlight all&#8221; into the action &#8220;find all&#8221;][19]
  * [replace old text snippet plugin with new one][20]
  * [optimize away nops for text editing][21]
  * [fixes in the template handler code][22]
  * [remove pythong browser plugin, since the symbol viewer plugin can do the same and much more][23]
  * [remove unmaintained html tools plugin, since it was never ported][24]
  * [optimization for bracket matching][25]
  * [new text snippet plugin: context aware snippet completion][26]
  * [fix filtering in the file browser plugin][27]
  * [always start search&replace from cursor][28]
  * [developer doumentation][29] [for smart range usage][30]
  * [cstyle indenter: fix indentation of multi-line comments][31]
  * [update of syntax highlighting for MySQL][32]
  * [performance optimization][33] [in the highlighting system:][34] [bug][35], [bug][36]
  * [fix crash in removeTrailingSpaces, if undo/redo is active][37]
  * [new interface for getting the mode at a given cursor][38]
  * search & replace: [lots of][39] [code][40] [cleanups][41]
  * [basic regression tests for search][42], [scripting][43], [and more][44]&#8230;
  * [scripting: add support for quoting in command line scripts][45]
  * [Kate crashes because of spell checking][46]
  * [fix regression: with auto brackets + selection][47]
  * [find-in-files: set filter correctly when opening the search][48]
  * [fix: make reload-scripts work with new functions][49]
  * [share a KCompletion object between all KateCmdLineEdits][50]

There are even more changes I left out. Most of those changes will be in KDE 4.4.1. If you want to help, join #kate in irc.libera.chat!

 [1]: http://ev.kde.org/activities/devmeetings/
 [2]: http://akademy.kde.org/
 [3]: http://techbase.kde.org/Contribute/Get_a_SVN_Account
 [4]: http://websvn.kde.org/?view=revision&revision=1091712
 [5]: http://websvn.kde.org/?view=revision&revision=1091718
 [6]: http://websvn.kde.org/?view=revision&revision=1091735
 [7]: https://bugs.kde.org/show_bug.cgi?id=227293
 [8]: https://bugs.kde.org/show_bug.cgi?id=190853
 [9]: https://bugs.kde.org/show_bug.cgi?id=225746
 [10]: http://websvn.kde.org/?view=revision&revision=1091115
 [11]: https://bugs.kde.org/show_bug.cgi?id=223624
 [12]: http://websvn.kde.org/?view=revision&revision=1091074
 [13]: https://bugs.kde.org/show_bug.cgi?id=196516
 [14]: http://websvn.kde.org/?view=revision&revision=1091041
 [15]: https://bugs.kde.org/show_bug.cgi?id=125104
 [16]: https://bugs.kde.org/show_bug.cgi?id=227008
 [17]: https://bugs.kde.org/show_bug.cgi?id=129494
 [18]: https://bugs.kde.org/show_bug.cgi?id=226409
 [19]: http://websvn.kde.org/?view=revision&revision=1090637
 [20]: http://websvn.kde.org/?view=revision&revision=1090635
 [21]: http://websvn.kde.org/?view=revision&revision=1090601
 [22]: http://websvn.kde.org/?view=revision&revision=1090598
 [23]: http://websvn.kde.org/?view=revision&revision=1090593
 [24]: http://websvn.kde.org/?view=revision&revision=1090592
 [25]: http://websvn.kde.org/?view=revision&revision=1090558
 [26]: http://websvn.kde.org/?view=revision&revision=1090515
 [27]: https://bugs.kde.org/show_bug.cgi?id=226529
 [28]: http://websvn.kde.org/?view=revision&revision=1090382
 [29]: http://websvn.kde.org/?view=revision&revision=1090377
 [30]: http://websvn.kde.org/?view=revision&revision=1090380
 [31]: https://bugs.kde.org/show_bug.cgi?id=169415
 [32]: http://websvn.kde.org/?view=revision&revision=1090241
 [33]: http://websvn.kde.org/?view=revision&revision=1090237
 [34]: http://websvn.kde.org/?view=revision&revision=1090238
 [35]: https://bugs.kde.org/show_bug.cgi?id=225228
 [36]: https://bugs.kde.org/show_bug.cgi?id=145686
 [37]: https://bugs.kde.org/show_bug.cgi?id=152203
 [38]: http://websvn.kde.org/?view=revision&revision=1090122
 [39]: http://websvn.kde.org/?view=revision&revision=1090106
 [40]: http://websvn.kde.org/?view=revision&revision=1090100
 [41]: http://websvn.kde.org/?view=revision&revision=1090103
 [42]: http://websvn.kde.org/?view=revision&revision=1090101
 [43]: http://websvn.kde.org/?view=revision&revision=1090102
 [44]: http://websvn.kde.org/?view=revision&revision=1090164
 [45]: http://websvn.kde.org/?view=revision&revision=1090086
 [46]: https://bugs.kde.org/show_bug.cgi?id=226724
 [47]: https://bugs.kde.org/show_bug.cgi?id=222181
 [48]: https://bugs.kde.org/show_bug.cgi?id=213096
 [49]: http://websvn.kde.org/?view=revision&revision=1089691
 [50]: http://websvn.kde.org/?view=revision&revision=1089690
