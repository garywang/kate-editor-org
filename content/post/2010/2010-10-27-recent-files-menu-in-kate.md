---
title: Recent Files Menu in Kate
author: Dominik Haumann

date: 2010-10-27T10:13:48+00:00
url: /2010/10/27/recent-files-menu-in-kate/
categories:
  - Users
tags:
  - planet

---
As many other applications Kate has the menu File > Recent Files, which lists all files you recently used. This recent file list right now is per session, i.e., each session has its own entries listed as recent files. Now there is a request to change it such that there is <a title="Recent Files in Kate" href="http://bugs.kde.org/show_bug.cgi?id=182759" target="_blank">just one single global recent file list</a>. I think having two recent files menus (one for the session and one global) is a bad solution. Further, adding an option for that also sounds wrong.

To change or not to change, that&#8217;s the question?! :) What do you think?