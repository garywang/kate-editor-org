---
title: 'Memory leak: Ui files and direct approach'
author: Dominik Haumann

date: 2007-11-21T22:38:00+00:00
url: /2007/11/21/memory-leak-ui-files-and-direct-approach/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2007/11/memory-leak-ui-files-and-direct.html
categories:
  - Developers

---
The KDE codebase often uses a forward declaration in the .h-file to speedup compilation. The code often looks like this:

<pre>// header file
namespace Ui { class MyWidget; }
class MyDialog : public KDialog {
  // ...
  private:
    Ui::MyWidget *ui;
};</pre>

The impl looks like this:

<pre>// source file
#include "mydialog.h"
#include "ui_mywidget.h"
MyDialog::MyDialog() : KDialog()
{
  QWidget *w = new QWidget(this);
  setMainWidget(w);
  ui = new Ui::MyWidget(); // allocation
  ui-&gt;setupUi(w);
  // ui-&gt;...
}</pre>

See the memory leak? <span style="font-weight: bold;">You have to call »delete ui;« in the destructor</span> if you use the »direct approach«. [Searching in lxr.kde.org][1] shows <span style="font-style: italic;">lots</span> of results, and in some places this delete is missing indeed. Happy fixing :)

* * *

<span style="font-weight: bold;">Update:</span> The really correct fix is to guard the pointer with an auto\_ptr or scoped\_ptr. For further details [read the][2] [comments below][3]. Or use [another approach to include your ui-file][4].</p> 

* * *If you do not want to delete it manually, you can for instance use a workaround like this:</p> 

<pre>// header file
namespace Ui { class MyWidget; }
<span style="font-weight: bold;">class MyWidget;</span>
class MyDialog : public KDialog {
  // ...
  private:
    Ui::MyWidget *ui;
};</pre>

Source file:

<pre>// source file
#include "mydialog.h"
#include "ui_mywidget.h"

<span style="font-weight: bold;">class MyWidget : public QWidget, public Ui::MyWidget {</span>
<span style="font-weight: bold;">public:</span>
<span style="font-weight: bold;">    MyWidget( QWidget * parent = 0 ) : QWidget( parent )</span>
<span style="font-weight: bold;">    { setupUi(this); }</span>
<span style="font-weight: bold;">};</span>

MyDialog::MyDialog() : KDialog()
{
  <span style="font-weight: bold;">ui = new MyWidget(this);</span>
  <span style="font-weight: bold;">setMainWidget(ui);</span>
  QWidget *w = new QWidget(this);
  setMainWidget(w);
  ui = new Ui::MyDialog(); // allocation
  ui-&gt;setupUi(w);
  // ui-&gt;...
}</pre>

 [1]: http://lxr.kde.org/ident?i=Ui
 [2]: http://dhaumann.blogspot.com/2007/11/memory-leak-ui-files-and-direct.html#c3247518843345483865
 [3]: http://dhaumann.blogspot.com/2007/11/memory-leak-ui-files-and-direct.html#c6385817874350497424
 [4]: http://doc.trolltech.com/4.3/designer-using-a-component.html