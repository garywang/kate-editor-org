---
title: KTextEditor - Small Things Matter - Continued
author: Christoph Cullmann
date: 2020-12-13T13:45:00+02:00
url: /post/2020/2020-12-13-small-things-matter-continued/
---

### Noise inside the terminal?

I more or less always start my Kate session inside a terminal, to let the project plugin auto-load the right one for the current working directory.

What I ignored for a long time is the noise this produces inside the terminal.

If you launch Kate or KWrite, you will often get stuff like (even for release builds):

<pre>
kf.sonnet.core: Unable to find any suggestion for "dummy to trigger identify"
kf.kio.core: We got some errors while running testparm "Weak crypto is allowed\nERROR: Invalid idmap range for domain *!"
</pre>

The first one stems from the language identification we trigger at startup to fix input lag for lazy loading of Sonnet things.

The second one stems from "testparm" execution to discover Samba shares.
If you have no Samba configured at all, this message will differ.

### Is this really wanted?

As nice this might be for debugging, I think for the normal user this is pointless.

No valid suggestion in Sonnet is just a valid API result, no need to inform the user on the terminal/log.

Same for Samba stuff:
In many cases the user not even configured that at all or isn't able to modify the potential "bad" Samba configuration.
In my case these settings are even intentional the way they are out of compatibility constraints in the local network.

There is really no point in filling either the terminal window or the session log with such stuff.

After a long time ignoring this, at least this noise is now removed:

* https://invent.kde.org/frameworks/sonnet/commit/535b8f0718ed848837748146a6b0b87f13cdfaee
* https://invent.kde.org/frameworks/kio/-/merge_requests/253

Same for some other kbuildsyscoca noise that sometimes happens, if you have auto-generated dummy files around:

* https://invent.kde.org/frameworks/kservice/-/merge_requests/32

Kate & KWrite (and stuff like Dolphin) now start up without any ugly noise (with current Frameworks from Git on a Manjaro system)!

### Can more be done?

With a disabled/non-functional Baloo I still get here for Dolphin or Kate with the file system browser plugin:

<pre>
kf.kio.core: "Could not enter folder tags:/."
</pre>

I yet need to find a way to remove this the "proper" way.
If somebody has some trivial fix for this, that would be welcome!

### Help welcome!

As you see, it is trivial to avoid such messages in the most cases.
If you are annoyed with such things, please provide patches on [invent.kde.org](https://invent.kde.org/) to improve the situation.
