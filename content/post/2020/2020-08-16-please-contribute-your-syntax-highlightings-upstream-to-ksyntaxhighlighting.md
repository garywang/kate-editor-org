---
title: Please contribute your syntax highlightings upstream to KSyntaxHighlighting
author: Christoph Cullmann
date: 2020-08-16T16:36:00+02:00
url: /post/2020/2020-08-16-please-contribute-your-syntax-highlightings-upstream-to-ksyntaxhighlighting/
---

Looking around the internet a bit for Kate related stuff, I stumbled over the current incarnation of the Haskell re-implementation of KSyntaxHighlighting: [skylighting](https://github.com/jgm/skylighting).
They reuse the highlighting definitions we provide and convert them to some Haskell data structures to do the highlighting.
[pandoc](https://pandoc.org/) uses this to provide [highlighting](https://pandoc.org/MANUAL.html#syntax-highlighting) for included code snippets.

Out of curiosity I wanted to know how up-to-date they are in respect to the files we keep in our repository, but interesting enough [their repository](https://github.com/jgm/skylighting/tree/master/skylighting-core/xml) actually contained some files we didn't have at all.

This was now rectified by importing the missing files to the official [KSyntaxHighlighting repository](https://invent.kde.org/frameworks/syntax-highlighting).
During that process, some minor faults detected by our compile time static checker for syntax files were fixed, too.

I contacted the skylighting maintainer and we will make it more obvious for the future that people should contribute new highlightings back to us, too.

Actually, if you search a bit on [GitHub](https://github.com/), you will find more highlighting files spread out in a lot of repositories.

Even more interesting is the stuff kept on [the KDE store](https://store.kde.org/browse/cat/472/order/latest/).
Inside the Kate App-Addons section there seems to be a mostly random collection of highlighting files rotting away since years without being submitted to us (with a few exceptions).

I strongly urge anyone that either creates a new highlighting definition for a language we not yet support or improve an existing one to please submit the changes upstream.

I can agree, that sometimes this makes no sense.
For example, if you have just adapted the highlighting to perfectly fit your personal needs/taste/style, it is reasonable to keep something locally.

But on the other side, if you went through the work to create a new one or add e.g. the latest language features to an existing one or even just fix one bug that is important for you, please contribute that back.

And contributing back means: please provide it to us to in-cooperate it into the official [KSyntaxHighlighting repository](https://invent.kde.org/frameworks/syntax-highlighting).
License wise we would prefer MIT for new files, but we will accept LGPLv2+ or similar licenses, too, if really needed.
For existing files, please keep the license they already have.

It is now easier than ever to contribute, just open a [merge request here](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests).
As you can see [here](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged), we already accepted a lot of such contributions in the past without any big hassle.

We prefer if you add some minimal example input for the language (or new language features) you provide, to have some better automated testing.
For details, just see [the following section of our short README.md](https://invent.kde.org/frameworks/syntax-highlighting/-/blob/master/README.md#how-to-contribute).

If you don't have the time to compile the stuff but have tested your file yourself, you can just provide the XML file and the example in your merge request and tell us that,
we can try it out and run the compile time checking and provide feedback, that will just make the submission process bit slower.

That said, please spend that short time necessary to give your improvements back to us and all our users.
You might make some people you don't even know happy ;=)

Beside this, we welcome any other contributions, e.g. to KSyntaxHighlighting, KTextEditor, Kate and Co.
For hints look at [this post about contributing via merge requests](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/).

If you have feedback, [use this thread on reddit](https://www.reddit.com/r/opensource/comments/iatfx2/please_contribute_your_syntax_highlightings/).
