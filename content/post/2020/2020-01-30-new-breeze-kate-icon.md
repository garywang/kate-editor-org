---
title: New Breeze Kate Icon
author: Christoph Cullmann
date: 2020-01-30T23:03:00+02:00
url: /post/2020/2020-01-30-new-breeze-kate-icon/
---

[Tyson Tan](https://tysontan.com/) provided now a Breeze variant of the [new Kate icon](/post/2020/2020-01-25-new-kate-icon/), too.

<p align="center">
    <a href="/images/kate.svg" target="_blank"><img width=512 src="/images/kate.svg"></a>
</p>

This is now in the master branch of the [breeze-icons](https://commits.kde.org/breeze-icons/b5df044e661452ad044465636c17598835ff7024) repository.

I hope other icon themes will update their Kate icon variants to match our new style, too.

I first tried to enforce the use of the new icon by renaming it, but I got reminded this is too harsh and I should give the icon theme authors a chance to update them in their own pace.

I guess that is the right approach and hope current themes will really catch up with this.

Thanks already in advance to all people that might spend time on this in the future.

We have both the Breeze and the non-Breeze variant as SVG files available in [our repository](https://invent.kde.org/utilities/kate/tree/master/kate/icons).
You can base your work on that, if you like.
For the licensing situation, take a look at the [previous post](/post/2020/2020-01-25-new-kate-icon/).

Thanks for the help during the submission process to Breeze to Tyson, Nate and Noah, see [D27044](https://phabricator.kde.org/D27044) and [T12594](https://phabricator.kde.org/T12594).
The documentation [how to create a proper Breeze icon](https://community.kde.org/Guidelines_and_HOWTOs/Icon_Workflow_Tips) is very useful, too!
