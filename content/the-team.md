---
title: The Team
hideMeta: true
author: Christoph Cullmann
date: 2010-07-09T09:03:57+00:00
menu:
  main:
    weight: 90
    parent: menu
---

## Who are the Kate & KWrite contributors?

[Kate & KWrite](https://invent.kde.org/utilities/kate) and the underlying [KTextEditor](https://invent.kde.org/frameworks/ktexteditor) & [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting) frameworks are created by a group of volunteers around the world.
The same is true for [this website](https://invent.kde.org/websites/kate-editor-org).

Below is a periodically updated list of the contributors to the Git repositories containing the above named software components & website.
As this list is purely based on the Git history of our repositories, it will be incomplete for otherwise submitted patches, etc.

The list is sorted by number of commits done by the individual contributors.
This is by no means the best measure of the impact of their contributions, but gives some rough estimation about their level of involvement.

## Don't forget the KDE Community!

Beside these explicitly named contributors to our text editor related components, a large portion of work is done by other members of the much broader [KDE community](https://kde.org/).

This includes crucial work like:

* developing the foundations we use, like KDE Frameworks
* doing all internationalization and translation work for our projects
* releasing our stuff
* maintaining our infrastructure (GitLab, CI, ...)
* supporting developer sprints

If you are not interested in [joining our text editor related team](/join-us/), please take a look if you want to [contribute to KDE](https://community.kde.org/Get_Involved).
The KDE community is a very welcoming bunch of people.

If you are not able to contribute, you might be interested to [donate](https://kde.org/donations).
Whereas these donations are not directly targeted at specific development, they help to keep the overall KDE community going.
For example some Kate related coding sprints were funded by the [KDE e.V.](https://ev.kde.org) based on these donations.

## Contributors during the last year
<b>101 people</b> contributed during the last year.
New contributors are highlighted, thanks for joining our team!

<table>
<tr>
<td>Christoph Cullmann <!-- 630 --></td>
<td>Waqar Ahmed <!-- 545 --></td>
</tr>
<tr>
<td><b>Pablo Rauzy</b> <!-- 130 --></td>
<td><b>Eric Armbruster</b> <!-- 79 --></td>
</tr>
<tr>
<td>loh tar <!-- 67 --></td>
<td>Jonathan Poelen <!-- 58 --></td>
</tr>
<tr>
<td>Ahmad Samir <!-- 53 --></td>
<td><b>Alain Laporte</b> <!-- 50 --></td>
</tr>
<tr>
<td>Kåre Särs <!-- 34 --></td>
<td>Laurent Montel <!-- 34 --></td>
</tr>
<tr>
<td>Volker Krause <!-- 29 --></td>
<td>Yuri Chornoivan <!-- 22 --></td>
</tr>
<tr>
<td>Mark Nauwelaerts <!-- 20 --></td>
<td>Nicolas Fella <!-- 20 --></td>
</tr>
<tr>
<td>Phu Nguyen <!-- 20 --></td>
<td>Alex Turbov <!-- 18 --></td>
</tr>
<tr>
<td><b>Georg Gadinger</b> <!-- 18 --></td>
<td>Friedrich W. H. Kossebau <!-- 17 --></td>
</tr>
<tr>
<td><b>Martin Seher</b> <!-- 16 --></td>
<td>Gary Wang <!-- 15 --></td>
</tr>
<tr>
<td>Alexander Lohnau <!-- 13 --></td>
<td><b>Bharadwaj Raju</b> <!-- 13 --></td>
</tr>
<tr>
<td>Heiko Becker <!-- 13 --></td>
<td>Milian Wolff <!-- 13 --></td>
</tr>
<tr>
<td><b>Giovana Vitor Dionisio Santana</b> <!-- 9 --></td>
<td>Héctor Mesa Jiménez <!-- 9 --></td>
</tr>
<tr>
<td><b>Jack Hill</b> <!-- 9 --></td>
<td>Eugene Popov <!-- 8 --></td>
</tr>
<tr>
<td><b>Rafał Lalik</b> <!-- 8 --></td>
<td><b>Adam Jimerson</b> <!-- 7 --></td>
</tr>
<tr>
<td>Albert Astals Cid <!-- 7 --></td>
<td>Alex Neundorf <!-- 7 --></td>
</tr>
<tr>
<td><b>Felipe Borges</b> <!-- 6 --></td>
<td><b>artyom kirnev</b> <!-- 6 --></td>
</tr>
<tr>
<td><b>Vítor Luís dos Santos Trindade</b> <!-- 5 --></td>
<td>Andreas Sturmlechner <!-- 4 --></td>
</tr>
<tr>
<td><b>Daniel Contreras</b> <!-- 4 --></td>
<td>David Faure <!-- 4 --></td>
</tr>
<tr>
<td>Harald Sitter <!-- 4 --></td>
<td>Jan Blackquill <!-- 4 --></td>
</tr>
<tr>
<td>Nate Graham <!-- 4 --></td>
<td><b>Sune Vuorela</b> <!-- 4 --></td>
</tr>
<tr>
<td><b>Alex Kh</b> <!-- 3 --></td>
<td>Amaury Bouchra Pilet <!-- 3 --></td>
</tr>
<tr>
<td><b>Brian Ward</b> <!-- 3 --></td>
<td>David Edmundson <!-- 3 --></td>
</tr>
<tr>
<td><b>Demmark Forrester</b> <!-- 3 --></td>
<td>Dominik Haumann <!-- 3 --></td>
</tr>
<tr>
<td><b>Ibrahim Abdullah</b> <!-- 3 --></td>
<td>Igor Kushnir <!-- 3 --></td>
</tr>
<tr>
<td>Jan Paul Batrina <!-- 3 --></td>
<td>Kai Uwe Broulik <!-- 3 --></td>
</tr>
<tr>
<td><b>Marco Rebhan</b> <!-- 3 --></td>
<td><b>shenleban tongying</b> <!-- 3 --></td>
</tr>
<tr>
<td>Andreas Gratzer <!-- 2 --></td>
<td><b>Christophe Dervieux</b> <!-- 2 --></td>
</tr>
<tr>
<td><b>Daniel Aleksandersen</b> <!-- 2 --></td>
<td>David Bryant <!-- 2 --></td>
</tr>
<tr>
<td>Fabian Kosmale <!-- 2 --></td>
<td>Kevin Funk <!-- 2 --></td>
</tr>
<tr>
<td>Mufeed Ali <!-- 2 --></td>
<td>Pino Toscano <!-- 2 --></td>
</tr>
<tr>
<td>Thomas Friedrichsmeier <!-- 2 --></td>
<td>Weng Xuetian <!-- 2 --></td>
</tr>
<tr>
<td><b>Aline Lermen</b> <!-- 1 --></td>
<td>Allen Winter <!-- 1 --></td>
</tr>
<tr>
<td><b>Beluga Whale</b> <!-- 1 --></td>
<td>Ben Cooksley <!-- 1 --></td>
</tr>
<tr>
<td><b>Ben Harless</b> <!-- 1 --></td>
<td>Christophe Giboudeaux <!-- 1 --></td>
</tr>
<tr>
<td><b>Davi Marinho</b> <!-- 1 --></td>
<td>David Redondo <!-- 1 --></td>
</tr>
<tr>
<td><b>Dawid Wróbel</b> <!-- 1 --></td>
<td><b>Fire Fragment</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Francisco Boni</b> <!-- 1 --></td>
<td><b>Frank Steinmetzger</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Fushan Wen</b> <!-- 1 --></td>
<td><b>Gaurav Shah</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Glebs Ivanovskis</b> <!-- 1 --></td>
<td><b>Gustavo  Rodrigues</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Helga Albert-Huszár</b> <!-- 1 --></td>
<td>Jakub Benda <!-- 1 --></td>
</tr>
<tr>
<td><b>Jan Bidler</b> <!-- 1 --></td>
<td><b>Janet Blackquill</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Javier Guerra</b> <!-- 1 --></td>
<td><b>Jonathan Lopez</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Karthik Nishanth</b> <!-- 1 --></td>
<td><b>Ksofix Alert</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Luis Taira</b> <!-- 1 --></td>
<td><b>Marián Konček</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Máté Pozsgay</b> <!-- 1 --></td>
<td><b>Ognian Milanov</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Philip K. Gisslow</b> <!-- 1 --></td>
<td><b>Quinten Kock</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Sam Greenwood</b> <!-- 1 --></td>
<td><b>Shalok Shalom</b> <!-- 1 --></td>
</tr>
<tr>
<td>Thiago Sueto <!-- 1 --></td>
<td><b>Wagner M Cunha</b> <!-- 1 --></td>
</tr>
<tr>
<td>Xaver Hugl <!-- 1 --></td>
<td><b>Yuhang Zhao</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>oldherl oh</b> <!-- 1 --></td>
</table>

## Contributors during the project lifetime
During the full project lifetime <b>663 people</b> contributed.
Thanks for making Kate possible!

<table>
<tr>
<td>Christoph Cullmann <!-- 6546 --></td>
<td>Dominik Haumann <!-- 2585 --></td>
</tr>
<tr>
<td>Waqar Ahmed <!-- 1177 --></td>
<td>Kåre Särs <!-- 540 --></td>
</tr>
<tr>
<td>Joseph Wenninger <!-- 538 --></td>
<td>Erlend Hamberg <!-- 514 --></td>
</tr>
<tr>
<td>Anders Lund <!-- 513 --></td>
<td>Hamish Rodda <!-- 482 --></td>
</tr>
<tr>
<td>Simon St James <!-- 456 --></td>
<td>Alex Turbov <!-- 421 --></td>
</tr>
<tr>
<td>Volker Krause <!-- 401 --></td>
<td>Laurent Montel <!-- 398 --></td>
</tr>
<tr>
<td>Milian Wolff <!-- 376 --></td>
<td>Michal Humpula <!-- 341 --></td>
</tr>
<tr>
<td>David Nolden <!-- 269 --></td>
<td>Mark Nauwelaerts <!-- 268 --></td>
</tr>
<tr>
<td>Jonathan Poelen <!-- 237 --></td>
<td>Bernhard Beschow <!-- 219 --></td>
</tr>
<tr>
<td>Shaheed Haque <!-- 208 --></td>
<td>David Faure <!-- 185 --></td>
</tr>
<tr>
<td>Nibaldo González <!-- 183 --></td>
<td>Albert Astals Cid <!-- 175 --></td>
</tr>
<tr>
<td>Friedrich W. H. Kossebau <!-- 165 --></td>
<td>Pascal Létourneau <!-- 165 --></td>
</tr>
<tr>
<td>Miquel Sabaté <!-- 163 --></td>
<td>Sven Brauch <!-- 155 --></td>
</tr>
<tr>
<td>Yuri Chornoivan <!-- 155 --></td>
<td>loh tar <!-- 148 --></td>
</tr>
<tr>
<td>Burkhard Lück <!-- 134 --></td>
<td>Pablo Rauzy <!-- 130 --></td>
</tr>
<tr>
<td>Ahmad Samir <!-- 124 --></td>
<td>Sebastian Pipping <!-- 121 --></td>
</tr>
<tr>
<td>Jan Paul Batrina <!-- 114 --></td>
<td>Pino Toscano <!-- 112 --></td>
</tr>
<tr>
<td>T.C. Hollingsworth <!-- 96 --></td>
<td>Christian Ehrlicher <!-- 93 --></td>
</tr>
<tr>
<td>Dirk Mueller <!-- 93 --></td>
<td>Robin Pedersen <!-- 93 --></td>
</tr>
<tr>
<td>Wilbert Berendsen <!-- 93 --></td>
<td>Pablo Martín <!-- 88 --></td>
</tr>
<tr>
<td>Michel Ludwig <!-- 87 --></td>
<td>Matthew Woehlke <!-- 84 --></td>
</tr>
<tr>
<td>Alex Neundorf <!-- 81 --></td>
<td>John Firebaugh <!-- 80 --></td>
</tr>
<tr>
<td>Eric Armbruster <!-- 79 --></td>
<td>Kevin Funk <!-- 74 --></td>
</tr>
<tr>
<td>Svyatoslav Kuzmich <!-- 66 --></td>
<td>Adrian Lungu <!-- 62 --></td>
</tr>
<tr>
<td>Alexander Lohnau <!-- 58 --></td>
<td>Christoph Feck <!-- 57 --></td>
</tr>
<tr>
<td>Christian Couder <!-- 56 --></td>
<td>Stephan Binner <!-- 56 --></td>
</tr>
<tr>
<td>Carl Schwan <!-- 55 --></td>
<td>Stephan Kulow <!-- 55 --></td>
</tr>
<tr>
<td>Mirko Stocker <!-- 54 --></td>
<td>Thomas Friedrichsmeier <!-- 53 --></td>
</tr>
<tr>
<td>Jesse Yurkovich <!-- 52 --></td>
<td>Alex Merry <!-- 51 --></td>
</tr>
<tr>
<td>Alain Laporte <!-- 50 --></td>
<td>Vegard Øye <!-- 49 --></td>
</tr>
<tr>
<td>Malcolm Hunter <!-- 46 --></td>
<td>Phu Nguyen <!-- 45 --></td>
</tr>
<tr>
<td>Alexander Neundorf <!-- 44 --></td>
<td>Gregor Mi <!-- 42 --></td>
</tr>
<tr>
<td>Montel Laurent <!-- 42 --></td>
<td>Nicolas Fella <!-- 41 --></td>
</tr>
<tr>
<td>Rafael Fernández López <!-- 38 --></td>
<td>Aleix Pol <!-- 35 --></td>
</tr>
<tr>
<td>Marco Mentasti <!-- 35 --></td>
<td>Simon Huerlimann <!-- 35 --></td>
</tr>
<tr>
<td>Allen Winter <!-- 33 --></td>
<td>Urs Wolfer <!-- 33 --></td>
</tr>
<tr>
<td>Heiko Becker <!-- 32 --></td>
<td>Phil Schaf <!-- 30 --></td>
</tr>
<tr>
<td>Igor Kushnir <!-- 29 --></td>
<td>Jakob Petsovits <!-- 29 --></td>
</tr>
<tr>
<td>Andrew Coles <!-- 28 --></td>
<td>Gerald Senarclens de Grancy <!-- 28 --></td>
</tr>
<tr>
<td>Adriaan de Groot <!-- 27 --></td>
<td>Chusslove Illich <!-- 27 --></td>
</tr>
<tr>
<td>Thomas Fjellstrom <!-- 27 --></td>
<td>Andreas Hartmetz <!-- 26 --></td>
</tr>
<tr>
<td>Martijn Klingens <!-- 26 --></td>
<td>Méven Car <!-- 26 --></td>
</tr>
<tr>
<td>André Wöbbeking <!-- 24 --></td>
<td>Alex Richardson <!-- 23 --></td>
</tr>
<tr>
<td>Frederik Schwarzer <!-- 22 --></td>
<td>Ilia Kats <!-- 22 --></td>
</tr>
<tr>
<td>Jarosław Staniek <!-- 22 --></td>
<td>Patrick Spendrin <!-- 22 --></td>
</tr>
<tr>
<td>Nicolas Goutte <!-- 21 --></td>
<td>Bram Schoenmakers <!-- 20 --></td>
</tr>
<tr>
<td>Simon Hausmann <!-- 20 --></td>
<td>Tomáš Trnka <!-- 20 --></td>
</tr>
<tr>
<td>Adrián Chaves Fernández (Gallaecio) <!-- 19 --></td>
<td>Aurélien Gâteau <!-- 19 --></td>
</tr>
<tr>
<td>Krzysztof Stokop <!-- 19 --></td>
<td>Marcell Fülöp <!-- 19 --></td>
</tr>
<tr>
<td>Aaron J. Seigo <!-- 18 --></td>
<td>Andreas Kling <!-- 18 --></td>
</tr>
<tr>
<td>Georg Gadinger <!-- 18 --></td>
<td>Kai Uwe Broulik <!-- 18 --></td>
</tr>
<tr>
<td>Arto Hytönen <!-- 17 --></td>
<td>Clarence Dang <!-- 17 --></td>
</tr>
<tr>
<td>Fabian Wunsch <!-- 17 --></td>
<td>Niko Sams <!-- 17 --></td>
</tr>
<tr>
<td>Paul Giannaros <!-- 17 --></td>
<td>Andrew Paseltiner <!-- 16 --></td>
</tr>
<tr>
<td>Brian Anderson <!-- 16 --></td>
<td>Gary Wang <!-- 16 --></td>
</tr>
<tr>
<td>Ian Reinhart Geiser <!-- 16 --></td>
<td>Martin Seher <!-- 16 --></td>
</tr>
<tr>
<td>Frederik Gladhorn <!-- 15 --></td>
<td>Kevin Ottens <!-- 15 --></td>
</tr>
<tr>
<td>Matt Rogers <!-- 15 --></td>
<td>Jan Blackquill <!-- 14 --></td>
</tr>
<tr>
<td>Luigi Toscano <!-- 14 --></td>
<td>Thiago Macieira <!-- 14 --></td>
</tr>
<tr>
<td>Bharadwaj Raju <!-- 13 --></td>
<td>Daniel Naber <!-- 13 --></td>
</tr>
<tr>
<td>Eike Hein <!-- 13 --></td>
<td>Héctor Mesa Jiménez <!-- 13 --></td>
</tr>
<tr>
<td>Jan Villat <!-- 13 --></td>
<td>Luca Beltrame <!-- 13 --></td>
</tr>
<tr>
<td>Nate Graham <!-- 13 --></td>
<td>Waldo Bastian <!-- 13 --></td>
</tr>
<tr>
<td>Andreas Cord-Landwehr <!-- 12 --></td>
<td>Christoph Roick <!-- 12 --></td>
</tr>
<tr>
<td>Daan De Meyer <!-- 12 --></td>
<td>Johannes Sixt <!-- 12 --></td>
</tr>
<tr>
<td>Jonathan Riddell <!-- 12 --></td>
<td>José Pablo Ezequiel Fernández <!-- 12 --></td>
</tr>
<tr>
<td>Leo Savernik <!-- 12 --></td>
<td>Andreas Pakulat <!-- 11 --></td>
</tr>
<tr>
<td>Andrey Matveyakin <!-- 11 --></td>
<td>Anthony Fieroni <!-- 11 --></td>
</tr>
<tr>
<td>Christophe Giboudeaux <!-- 11 --></td>
<td>Chusslove Illich (Часлав Илић) <!-- 11 --></td>
</tr>
<tr>
<td>David Edmundson <!-- 11 --></td>
<td>Diego Iastrubni <!-- 11 --></td>
</tr>
<tr>
<td>Philipp A <!-- 11 --></td>
<td>Tobias Koenig <!-- 11 --></td>
</tr>
<tr>
<td>Alexander Potashev <!-- 10 --></td>
<td>Andreas Gratzer <!-- 10 --></td>
</tr>
<tr>
<td>Eduardo Robles Elvira <!-- 10 --></td>
<td>Harald Fernengel <!-- 10 --></td>
</tr>
<tr>
<td>Jiri Pinkava <!-- 10 --></td>
<td>Jonathan Schmidt-Dominé <!-- 10 --></td>
</tr>
<tr>
<td>Martin Walch <!-- 10 --></td>
<td>Matthias Kretz <!-- 10 --></td>
</tr>
<tr>
<td>Tomaz Canabrava <!-- 10 --></td>
<td>Trevor Blight <!-- 10 --></td>
</tr>
<tr>
<td>Wang Kai <!-- 10 --></td>
<td>David Bryant <!-- 9 --></td>
</tr>
<tr>
<td>Ellis Whitehead <!-- 9 --></td>
<td>Eugene Popov <!-- 9 --></td>
</tr>
<tr>
<td>Giovana Vitor Dionisio Santana <!-- 9 --></td>
<td>Harsh Kumar <!-- 9 --></td>
</tr>
<tr>
<td>Jack Hill <!-- 9 --></td>
<td>Jaime Torres <!-- 9 --></td>
</tr>
<tr>
<td>Lasse Liehu <!-- 9 --></td>
<td>Maks Orlovich <!-- 9 --></td>
</tr>
<tr>
<td>Oswald Buddenhagen <!-- 9 --></td>
<td>Andras Mantia <!-- 8 --></td>
</tr>
<tr>
<td>Christian Loose <!-- 8 --></td>
<td>David Palser <!-- 8 --></td>
</tr>
<tr>
<td>Dmitry Risenberg <!-- 8 --></td>
<td>Frank Osterfeld <!-- 8 --></td>
</tr>
<tr>
<td>John Layt <!-- 8 --></td>
<td>John Tapsell <!-- 8 --></td>
</tr>
<tr>
<td>Oliver Kellogg <!-- 8 --></td>
<td>Primoz Anzur <!-- 8 --></td>
</tr>
<tr>
<td>Rafał Lalik <!-- 8 --></td>
<td>Ralf Habacker <!-- 8 --></td>
</tr>
<tr>
<td>René J.V. Bertin <!-- 8 --></td>
<td>Weng Xuetian <!-- 8 --></td>
</tr>
<tr>
<td>flying sheep <!-- 8 --></td>
<td>Abhishek Patil <!-- 7 --></td>
</tr>
<tr>
<td>Adam Jimerson <!-- 7 --></td>
<td>Anne-Marie Mahfouf <!-- 7 --></td>
</tr>
<tr>
<td>Bastian Holst <!-- 7 --></td>
<td>Ben Cooksley <!-- 7 --></td>
</tr>
<tr>
<td>David Jarvie <!-- 7 --></td>
<td>David Redondo <!-- 7 --></td>
</tr>
<tr>
<td>David Schulz <!-- 7 --></td>
<td>Filip Gawin <!-- 7 --></td>
</tr>
<tr>
<td>George Staikos <!-- 7 --></td>
<td>Hannah von Reth <!-- 7 --></td>
</tr>
<tr>
<td>Harald Sitter <!-- 7 --></td>
<td>Jeroen Wijnhout <!-- 7 --></td>
</tr>
<tr>
<td>John Salatas <!-- 7 --></td>
<td>Kazuki Ohta <!-- 7 --></td>
</tr>
<tr>
<td>Matt Broadstone <!-- 7 --></td>
<td>Orgad Shaneh <!-- 7 --></td>
</tr>
<tr>
<td>Peter Oberndorfer <!-- 7 --></td>
<td>Ryan Cumming <!-- 7 --></td>
</tr>
<tr>
<td>Yury G. Kudryashov <!-- 7 --></td>
<td>Adam Treat <!-- 6 --></td>
</tr>
<tr>
<td>Andreas Sturmlechner <!-- 6 --></td>
<td>Andrzej Dabrowski <!-- 6 --></td>
</tr>
<tr>
<td>Charles Samuels <!-- 6 --></td>
<td>Daniel Levin <!-- 6 --></td>
</tr>
<tr>
<td>Dawit Alemayehu <!-- 6 --></td>
<td>Denis Doria <!-- 6 --></td>
</tr>
<tr>
<td>Felipe Borges <!-- 6 --></td>
<td>Gleb Popov <!-- 6 --></td>
</tr>
<tr>
<td>Ian Wakeling <!-- 6 --></td>
<td>Jaison Lee <!-- 6 --></td>
</tr>
<tr>
<td>Jos van den Oever <!-- 6 --></td>
<td>Lukáš Tinkl <!-- 6 --></td>
</tr>
<tr>
<td>Martin T. H. Sandsmark <!-- 6 --></td>
<td>Massimo Callegari <!-- 6 --></td>
</tr>
<tr>
<td>Michael Jansen <!-- 6 --></td>
<td>Nathaniel Graham <!-- 6 --></td>
</tr>
<tr>
<td>Nick Shaforostoff <!-- 6 --></td>
<td>Nicolás Alvarez <!-- 6 --></td>
</tr>
<tr>
<td>Olivier Goffart <!-- 6 --></td>
<td>R.J.V. Bertin <!-- 6 --></td>
</tr>
<tr>
<td>Rafał Rzepecki <!-- 6 --></td>
<td>Robert Hoffmann <!-- 6 --></td>
</tr>
<tr>
<td>Safa AlFulaij <!-- 6 --></td>
<td>Samuel Gaist <!-- 6 --></td>
</tr>
<tr>
<td>Shubham Jangra <!-- 6 --></td>
<td>York Xiang <!-- 6 --></td>
</tr>
<tr>
<td>artyom kirnev <!-- 6 --></td>
<td>Allan Sandfeld Jensen <!-- 5 --></td>
</tr>
<tr>
<td>Amaury Bouchra Pilet <!-- 5 --></td>
<td>Andreas Holzammer <!-- 5 --></td>
</tr>
<tr>
<td>Arno Rehn <!-- 5 --></td>
<td>Bernd Gehrmann <!-- 5 --></td>
</tr>
<tr>
<td>Carlo Segato <!-- 5 --></td>
<td>Carsten Pfeiffer <!-- 5 --></td>
</tr>
<tr>
<td>Charles Vejnar <!-- 5 --></td>
<td>David Herberth <!-- 5 --></td>
</tr>
<tr>
<td>Denis Lisov <!-- 5 --></td>
<td>Harri Porten <!-- 5 --></td>
</tr>
<tr>
<td>Harsh Chouraria J <!-- 5 --></td>
<td>Helio Castro <!-- 5 --></td>
</tr>
<tr>
<td>Heng Liu <!-- 5 --></td>
<td>Holger Danielsson <!-- 5 --></td>
</tr>
<tr>
<td>Ivan Čukić <!-- 5 --></td>
<td>Ivo Anjo <!-- 5 --></td>
</tr>
<tr>
<td>Michael Hansen <!-- 5 --></td>
<td>Mufeed Ali <!-- 5 --></td>
</tr>
<tr>
<td>Peter Kümmel <!-- 5 --></td>
<td>Richard Smith <!-- 5 --></td>
</tr>
<tr>
<td>Scott Wheeler <!-- 5 --></td>
<td>Stephen Kelly <!-- 5 --></td>
</tr>
<tr>
<td>Thomas Braun <!-- 5 --></td>
<td>Vítor Luís dos Santos Trindade <!-- 5 --></td>
</tr>
<tr>
<td>Xaver Hugl <!-- 5 --></td>
<td>andreas kainz <!-- 5 --></td>
</tr>
<tr>
<td>Andi Fischer <!-- 4 --></td>
<td>Andrius Štikonas <!-- 4 --></td>
</tr>
<tr>
<td>Antonio Rojas <!-- 4 --></td>
<td>Benjamin Meyer <!-- 4 --></td>
</tr>
<tr>
<td>Daniel Contreras <!-- 4 --></td>
<td>Darío Andrés Rodríguez <!-- 4 --></td>
</tr>
<tr>
<td>Dominique Devriese <!-- 4 --></td>
<td>Emeric Dupont <!-- 4 --></td>
</tr>
<tr>
<td>Evgeniy Ivanov <!-- 4 --></td>
<td>Fabian Kosmale <!-- 4 --></td>
</tr>
<tr>
<td>Flavio Castelli <!-- 4 --></td>
<td>Grzegorz Szymaszek <!-- 4 --></td>
</tr>
<tr>
<td>Guo Yunhe <!-- 4 --></td>
<td>Helio Chissini de Castro <!-- 4 --></td>
</tr>
<tr>
<td>Luboš Luňák <!-- 4 --></td>
<td>Michael Pyne <!-- 4 --></td>
</tr>
<tr>
<td>Nadeem Hasan <!-- 4 --></td>
<td>Nikita Sirgienko <!-- 4 --></td>
</tr>
<tr>
<td>Rolf Eike Beer <!-- 4 --></td>
<td>Stefan Asserhäll <!-- 4 --></td>
</tr>
<tr>
<td>Sune Vuorela <!-- 4 --></td>
<td>Thomas Schoeps <!-- 4 --></td>
</tr>
<tr>
<td>Toshitaka Fujioka <!-- 4 --></td>
<td>Aaron Puchert <!-- 3 --></td>
</tr>
<tr>
<td>Alex Crichton <!-- 3 --></td>
<td>Alex Kh <!-- 3 --></td>
</tr>
<tr>
<td>Bernhard Loos <!-- 3 --></td>
<td>Boris Egorov <!-- 3 --></td>
</tr>
<tr>
<td>Brian Ward <!-- 3 --></td>
<td>Daniele Scasciafratte <!-- 3 --></td>
</tr>
<tr>
<td>Dave Corrie <!-- 3 --></td>
<td>David Leimbach <!-- 3 --></td>
</tr>
<tr>
<td>Davide Bettio <!-- 3 --></td>
<td>Demmark Forrester <!-- 3 --></td>
</tr>
<tr>
<td>Dāvis Mosāns <!-- 3 --></td>
<td>Ernesto Castellotti <!-- 3 --></td>
</tr>
<tr>
<td>Francis Laniel <!-- 3 --></td>
<td>Helge Deller <!-- 3 --></td>
</tr>
<tr>
<td>Holger Schröder <!-- 3 --></td>
<td>Ibrahim Abdullah <!-- 3 --></td>
</tr>
<tr>
<td>Ignacio Castaño Aguado <!-- 3 --></td>
<td>Ilya Konstantinov <!-- 3 --></td>
</tr>
<tr>
<td>Jacob Rideout <!-- 3 --></td>
<td>Laurence Withers <!-- 3 --></td>
</tr>
<tr>
<td>Marco Rebhan <!-- 3 --></td>
<td>Marcus Camen <!-- 3 --></td>
</tr>
<tr>
<td>Markus Meik Slopianka <!-- 3 --></td>
<td>Martin Kostolný <!-- 3 --></td>
</tr>
<tr>
<td>Michael Goffioul <!-- 3 --></td>
<td>Mickael Marchand <!-- 3 --></td>
</tr>
<tr>
<td>Mike Harris <!-- 3 --></td>
<td>Raymond Wooninck <!-- 3 --></td>
</tr>
<tr>
<td>Rex Dieter <!-- 3 --></td>
<td>Reza Arbab <!-- 3 --></td>
</tr>
<tr>
<td>Richard Dale <!-- 3 --></td>
<td>Richard J. Moore <!-- 3 --></td>
</tr>
<tr>
<td>Robert Knight <!-- 3 --></td>
<td>Sergio Martins <!-- 3 --></td>
</tr>
<tr>
<td>Simon Persson <!-- 3 --></td>
<td>Thiago Sueto <!-- 3 --></td>
</tr>
<tr>
<td>Vladimir Prus <!-- 3 --></td>
<td>Wes H <!-- 3 --></td>
</tr>
<tr>
<td>Willy De la Court <!-- 3 --></td>
<td>Wolfgang Bauer <!-- 3 --></td>
</tr>
<tr>
<td>shenleban tongying <!-- 3 --></td>
<td>Ömer Fadıl USTA <!-- 3 --></td>
</tr>
<tr>
<td>Ömer Fadıl Usta <!-- 3 --></td>
<td>Ada Sauce <!-- 2 --></td>
</tr>
<tr>
<td>Alexander Dymo <!-- 2 --></td>
<td>Alexander Zhigalin <!-- 2 --></td>
</tr>
<tr>
<td>Andrew Crouthamel <!-- 2 --></td>
<td>Antonio Larrosa Jimenez <!-- 2 --></td>
</tr>
<tr>
<td>Arek Koz (Arusekk) <!-- 2 --></td>
<td>Arend van Beelen jr <!-- 2 --></td>
</tr>
<tr>
<td>Bernhard Rosenkraenzer <!-- 2 --></td>
<td>Bhushan Shah <!-- 2 --></td>
</tr>
<tr>
<td>Caleb Tennis <!-- 2 --></td>
<td>Casper Boemann <!-- 2 --></td>
</tr>
<tr>
<td>Chris Howells <!-- 2 --></td>
<td>Christophe Dervieux <!-- 2 --></td>
</tr>
<tr>
<td>Christopher Yeleighton <!-- 2 --></td>
<td>Cornelius Schumacher <!-- 2 --></td>
</tr>
<tr>
<td>Craig Drummond <!-- 2 --></td>
<td>Cristian Oneț <!-- 2 --></td>
</tr>
<tr>
<td>Cédric Borgese <!-- 2 --></td>
<td>Daniel Aleksandersen <!-- 2 --></td>
</tr>
<tr>
<td>Daniel Tang <!-- 2 --></td>
<td>Dmitry Suzdalev <!-- 2 --></td>
</tr>
<tr>
<td>Elvis Angelaccio <!-- 2 --></td>
<td>Enrique Matías Sánchez <!-- 2 --></td>
</tr>
<tr>
<td>Fabian Vogt <!-- 2 --></td>
<td>Frans Englich <!-- 2 --></td>
</tr>
<tr>
<td>Frerich Raabe <!-- 2 --></td>
<td>Gene Thomas <!-- 2 --></td>
</tr>
<tr>
<td>George Florea Bănuș <!-- 2 --></td>
<td>Gioele Barabucci <!-- 2 --></td>
</tr>
<tr>
<td>Gregory S. Hayes <!-- 2 --></td>
<td>Heinz Wiesinger <!-- 2 --></td>
</tr>
<tr>
<td>Hoàng Đức Hiếu <!-- 2 --></td>
<td>Hugo Pereira Da Costa <!-- 2 --></td>
</tr>
<tr>
<td>Huon Wilson <!-- 2 --></td>
<td>Ivan Shapovalov <!-- 2 --></td>
</tr>
<tr>
<td>Jaime Torres Amate <!-- 2 --></td>
<td>Jakub Benda <!-- 2 --></td>
</tr>
<tr>
<td>Jekyll Wu <!-- 2 --></td>
<td>Jens Dagerbo <!-- 2 --></td>
</tr>
<tr>
<td>Julien Antille <!-- 2 --></td>
<td>Jure Repinc <!-- 2 --></td>
</tr>
<tr>
<td>Kurt Pfeifle <!-- 2 --></td>
<td>Leonardo Finetti <!-- 2 --></td>
</tr>
<tr>
<td>Marcello Massaro <!-- 2 --></td>
<td>Mario Aichinger <!-- 2 --></td>
</tr>
<tr>
<td>Markus Brenneis <!-- 2 --></td>
<td>Markus Pister <!-- 2 --></td>
</tr>
<tr>
<td>Markus Slopianka <!-- 2 --></td>
<td>Maximilian Löffler <!-- 2 --></td>
</tr>
<tr>
<td>Michael Palimaka <!-- 2 --></td>
<td>Michel Hermier <!-- 2 --></td>
</tr>
<tr>
<td>Mickael Bosch <!-- 2 --></td>
<td>Miklos Marton <!-- 2 --></td>
</tr>
<tr>
<td>Nikolas Zimmermann <!-- 2 --></td>
<td>Petter Stokke <!-- 2 --></td>
</tr>
<tr>
<td>Phil Young <!-- 2 --></td>
<td>Pierre-Marie Pédrot <!-- 2 --></td>
</tr>
<tr>
<td>Raphael Kubo da Costa <!-- 2 --></td>
<td>Rob Buis <!-- 2 --></td>
</tr>
<tr>
<td>Roberto Raggi <!-- 2 --></td>
<td>Scott Lawrence <!-- 2 --></td>
</tr>
<tr>
<td>Sebastian Kügler <!-- 2 --></td>
<td>Sebastian Sauer <!-- 2 --></td>
</tr>
<tr>
<td>Sergey Kalinichev <!-- 2 --></td>
<td>Shaun Reich <!-- 2 --></td>
</tr>
<tr>
<td>Silas Lenz <!-- 2 --></td>
<td>Sven Leiber <!-- 2 --></td>
</tr>
<tr>
<td>Thomas Braxton <!-- 2 --></td>
<td>Thomas Häber <!-- 2 --></td>
</tr>
<tr>
<td>Thomas Leitner <!-- 2 --></td>
<td>Thomas Surrel <!-- 2 --></td>
</tr>
<tr>
<td>Thorsten Roeder <!-- 2 --></td>
<td>Tim Beaulen <!-- 2 --></td>
</tr>
<tr>
<td>Tim Hutt <!-- 2 --></td>
<td>Tobias C. Berner <!-- 2 --></td>
</tr>
<tr>
<td>Valentin Rouet <!-- 2 --></td>
<td>Vincent Belliard <!-- 2 --></td>
</tr>
<tr>
<td>Vincenzo Buttazzo <!-- 2 --></td>
<td>Volker Augustin <!-- 2 --></td>
</tr>
<tr>
<td>Will Entriken <!-- 2 --></td>
<td>William Wold <!-- 2 --></td>
</tr>
<tr>
<td>Zack Rusin <!-- 2 --></td>
<td>Алексей Шилин <!-- 2 --></td>
</tr>
<tr>
<td>Aaron Seigo <!-- 1 --></td>
<td>Alberto Salvia Novella <!-- 1 --></td>
</tr>
<tr>
<td>Aleix Pol Gonzalez <!-- 1 --></td>
<td>Alex Hermann <!-- 1 --></td>
</tr>
<tr>
<td>Alexander Schlarb <!-- 1 --></td>
<td>Alexander Volkov <!-- 1 --></td>
</tr>
<tr>
<td>Alexey Bogdanenko <!-- 1 --></td>
<td>Aline Lermen <!-- 1 --></td>
</tr>
<tr>
<td>Amit Kumar Jaiswal <!-- 1 --></td>
<td>Ana Beatriz Guerrero López <!-- 1 --></td>
</tr>
<tr>
<td>Anakim Border <!-- 1 --></td>
<td>Anders Ponga <!-- 1 --></td>
</tr>
<tr>
<td>Andre Heinecke <!-- 1 --></td>
<td>Andrea Canciani <!-- 1 --></td>
</tr>
<tr>
<td>Andrea Scarpino <!-- 1 --></td>
<td>Andreas Abel <!-- 1 --></td>
</tr>
<tr>
<td>Andreas Hohenegger <!-- 1 --></td>
<td>Andreas Simon <!-- 1 --></td>
</tr>
<tr>
<td>Andrew Chen <!-- 1 --></td>
<td>Andrey Karepin <!-- 1 --></td>
</tr>
<tr>
<td>Andrey S. Cherepanov <!-- 1 --></td>
<td>Andrius da Costa Ribas <!-- 1 --></td>
</tr>
<tr>
<td>André Marcelo Alvarenga <!-- 1 --></td>
<td>Andy Goossens <!-- 1 --></td>
</tr>
<tr>
<td>Antoni Bella Pérez <!-- 1 --></td>
<td>Arctic Ice Studio <!-- 1 --></td>
</tr>
<tr>
<td>Arnaud Ruiz <!-- 1 --></td>
<td>Arnold Dumas <!-- 1 --></td>
</tr>
<tr>
<td>Arnold Krille <!-- 1 --></td>
<td>Ashish Bansal <!-- 1 --></td>
</tr>
<tr>
<td>Axel Kittenberger <!-- 1 --></td>
<td>Ayushmaan jangid <!-- 1 --></td>
</tr>
<tr>
<td>Azat Khuzhin <!-- 1 --></td>
<td>Bart Ribbers <!-- 1 --></td>
</tr>
<tr>
<td>Barış Metin <!-- 1 --></td>
<td>Beluga Whale <!-- 1 --></td>
</tr>
<tr>
<td>Ben Blum <!-- 1 --></td>
<td>Ben Harless <!-- 1 --></td>
</tr>
<tr>
<td>Benjamin Buch <!-- 1 --></td>
<td>Benjamin C Meyer <!-- 1 --></td>
</tr>
<tr>
<td>Bernd Buschinski <!-- 1 --></td>
<td>Björn Peemöller <!-- 1 --></td>
</tr>
<tr>
<td>Boris Petrov <!-- 1 --></td>
<td>Brad Hards <!-- 1 --></td>
</tr>
<tr>
<td>Brendan Zabarauskas <!-- 1 --></td>
<td>Bruno Virlet <!-- 1 --></td>
</tr>
<tr>
<td>Casper van Donderen <!-- 1 --></td>
<td>Christoph Rüßler <!-- 1 --></td>
</tr>
<tr>
<td>Christopher Blauvelt <!-- 1 --></td>
<td>Claudio Bantaloukas <!-- 1 --></td>
</tr>
<tr>
<td>Conrad Hoffmann <!-- 1 --></td>
<td>Corey Richardson <!-- 1 --></td>
</tr>
<tr>
<td>Cédric Pasteur <!-- 1 --></td>
<td>Dan Vrátil <!-- 1 --></td>
</tr>
<tr>
<td>Daniel Laidig <!-- 1 --></td>
<td>Daniel Micay <!-- 1 --></td>
</tr>
<tr>
<td>Daniel Sonck <!-- 1 --></td>
<td>Davi Marinho <!-- 1 --></td>
</tr>
<tr>
<td>David Rosca <!-- 1 --></td>
<td>David Smith <!-- 1 --></td>
</tr>
<tr>
<td>Dawid Wróbel <!-- 1 --></td>
<td>Denis Steckelmacher <!-- 1 --></td>
</tr>
<tr>
<td>Diana-Victoria Tiriplica <!-- 1 --></td>
<td>Diggory Hardy <!-- 1 --></td>
</tr>
<tr>
<td>Dirk Rathlev <!-- 1 --></td>
<td>Duncan Mac-Vicar Prett <!-- 1 --></td>
</tr>
<tr>
<td>Ede Rag <!-- 1 --></td>
<td>Ederag <!-- 1 --></td>
</tr>
<tr>
<td>Elias Probst <!-- 1 --></td>
<td>Emanuele Tamponi <!-- 1 --></td>
</tr>
<tr>
<td>Emmanuel Lepage Vallee <!-- 1 --></td>
<td>Ewald Snel <!-- 1 --></td>
</tr>
<tr>
<td>Federico Zenith <!-- 1 --></td>
<td>Felipe Kinoshita <!-- 1 --></td>
</tr>
<tr>
<td>Felix Yan <!-- 1 --></td>
<td>Filipe Saraiva <!-- 1 --></td>
</tr>
<tr>
<td>Fire Fragment <!-- 1 --></td>
<td>Francis Herne <!-- 1 --></td>
</tr>
<tr>
<td>Francisco Boni <!-- 1 --></td>
<td>Francois-Xavier Duranceau <!-- 1 --></td>
</tr>
<tr>
<td>Frank Steinmetzger <!-- 1 --></td>
<td>Frederik Banning <!-- 1 --></td>
</tr>
<tr>
<td>Fredrik Höglund <!-- 1 --></td>
<td>Fushan Wen <!-- 1 --></td>
</tr>
<tr>
<td>Gaurav Shah <!-- 1 --></td>
<td>Germain Garand <!-- 1 --></td>
</tr>
<tr>
<td>Glebs Ivanovskis <!-- 1 --></td>
<td>Gregor Tätzner <!-- 1 --></td>
</tr>
<tr>
<td>Guillermo Antonio Amaral Bastidas <!-- 1 --></td>
<td>Guillermo Molteni <!-- 1 --></td>
</tr>
<tr>
<td>Gustavo  Rodrigues <!-- 1 --></td>
<td>Hartmut Goebel <!-- 1 --></td>
</tr>
<tr>
<td>Helga Albert-Huszár <!-- 1 --></td>
<td>HeroesGrave <!-- 1 --></td>
</tr>
<tr>
<td>Ian Monroe <!-- 1 --></td>
<td>Ignat Semenov <!-- 1 --></td>
</tr>
<tr>
<td>Ismael Asensio <!-- 1 --></td>
<td>Ivan Koveshnikov <!-- 1 --></td>
</tr>
<tr>
<td>Jan Bidler <!-- 1 --></td>
<td>Jan Grulich <!-- 1 --></td>
</tr>
<tr>
<td>Jan Przybylak <!-- 1 --></td>
<td>Janet Blackquill <!-- 1 --></td>
</tr>
<tr>
<td>Javier Guerra <!-- 1 --></td>
<td>Jeremy Whiting <!-- 1 --></td>
</tr>
<tr>
<td>Jesse Crossen <!-- 1 --></td>
<td>Jiří Wolker <!-- 1 --></td>
</tr>
<tr>
<td>Jochen Wilhelmy <!-- 1 --></td>
<td>Joerg Schiermeier <!-- 1 --></td>
</tr>
<tr>
<td>John Schroeder <!-- 1 --></td>
<td>Johnny Jazeix <!-- 1 --></td>
</tr>
<tr>
<td>Jonathan L. Verner <!-- 1 --></td>
<td>Jonathan Lopez <!-- 1 --></td>
</tr>
<tr>
<td>Jonathan Marten <!-- 1 --></td>
<td>Jonathan Raphael Joachim Kolberg <!-- 1 --></td>
</tr>
<tr>
<td>Jonathan Singer <!-- 1 --></td>
<td>Jonathan Verner <!-- 1 --></td>
</tr>
<tr>
<td>José Joaquín Atria <!-- 1 --></td>
<td>Juan Francisco Cantero Hurtado <!-- 1 --></td>
</tr>
<tr>
<td>Juliano F. Ravasi <!-- 1 --></td>
<td>Juraj Oravec <!-- 1 --></td>
</tr>
<tr>
<td>Karol Szwed <!-- 1 --></td>
<td>Karthik Nishanth <!-- 1 --></td>
</tr>
<tr>
<td>Kevin Ballard <!-- 1 --></td>
<td>Kishore Gopalakrishnan <!-- 1 --></td>
</tr>
<tr>
<td>Ksofix Alert <!-- 1 --></td>
<td>Kurt Granroth <!-- 1 --></td>
</tr>
<tr>
<td>Kurt Hindenburg <!-- 1 --></td>
<td>Kyle S Horne <!-- 1 --></td>
</tr>
<tr>
<td>Laurent Cimon <!-- 1 --></td>
<td>Lauri Watts <!-- 1 --></td>
</tr>
<tr>
<td>Lays Rodrigues <!-- 1 --></td>
<td>Leandro Emanuel López <!-- 1 --></td>
</tr>
<tr>
<td>Leandro Santiago <!-- 1 --></td>
<td>Li-yao Xia <!-- 1 --></td>
</tr>
<tr>
<td>Liu Zhe <!-- 1 --></td>
<td>Luis Taira <!-- 1 --></td>
</tr>
<tr>
<td>Maciej Mrozowski <!-- 1 --></td>
<td>Magnus Hoff <!-- 1 --></td>
</tr>
<tr>
<td>Malte Starostik <!-- 1 --></td>
<td>Manuel Tortosa <!-- 1 --></td>
</tr>
<tr>
<td>Marc Espie <!-- 1 --></td>
<td>Marco Martin <!-- 1 --></td>
</tr>
<tr>
<td>Marijn Kruisselbrink <!-- 1 --></td>
<td>Marián Konček <!-- 1 --></td>
</tr>
<tr>
<td>Markus Ebner <!-- 1 --></td>
<td>Martin Gräßlin <!-- 1 --></td>
</tr>
<tr>
<td>Martin Klapetek <!-- 1 --></td>
<td>Martin Sandsmark <!-- 1 --></td>
</tr>
<tr>
<td>Martin Tobias Holmedahl Sandsmark <!-- 1 --></td>
<td>Marvin Ahlgrimm <!-- 1 --></td>
</tr>
<tr>
<td>Matheus C. França <!-- 1 --></td>
<td>Matt Carberry <!-- 1 --></td>
</tr>
<tr>
<td>Matthias Gerstner <!-- 1 --></td>
<td>Matthias Hoelzer-Kluepfel <!-- 1 --></td>
</tr>
<tr>
<td>Matthias Klumpp <!-- 1 --></td>
<td>Melchior Franz <!-- 1 --></td>
</tr>
<tr>
<td>Michael Brade <!-- 1 --></td>
<td>Michael Drueing <!-- 1 --></td>
</tr>
<tr>
<td>Michael Heidelbach <!-- 1 --></td>
<td>Michael Matz <!-- 1 --></td>
</tr>
<tr>
<td>Michael Ritzert <!-- 1 --></td>
<td>Michal Srb <!-- 1 --></td>
</tr>
<tr>
<td>Mikhail Zolotukhin <!-- 1 --></td>
<td>Mikko Perttunen <!-- 1 --></td>
</tr>
<tr>
<td>Miquel Sabaté Solà <!-- 1 --></td>
<td>Momo Cao <!-- 1 --></td>
</tr>
<tr>
<td>Máté Pozsgay <!-- 1 --></td>
<td>Médéric Boquien <!-- 1 --></td>
</tr>
<tr>
<td>Nazar Kalinowski <!-- 1 --></td>
<td>Nguyen Hung Phu <!-- 1 --></td>
</tr>
<tr>
<td>Nico Kruber <!-- 1 --></td>
<td>Nicola Gigante <!-- 1 --></td>
</tr>
<tr>
<td>Nicolas Lécureuil <!-- 1 --></td>
<td>Nikolay Kultashev <!-- 1 --></td>
</tr>
<tr>
<td>Ognian Milanov <!-- 1 --></td>
<td>Oleksandr Senkovych <!-- 1 --></td>
</tr>
<tr>
<td>Oliver Sander <!-- 1 --></td>
<td>Olivier CHURLAUD <!-- 1 --></td>
</tr>
<tr>
<td>Olivier Felt <!-- 1 --></td>
<td>Ovidiu-Florin BOGDAN <!-- 1 --></td>
</tr>
<tr>
<td>Pani Ram <!-- 1 --></td>
<td>Paolo Borelli <!-- 1 --></td>
</tr>
<tr>
<td>Parker Coates <!-- 1 --></td>
<td>Pat Brown <!-- 1 --></td>
</tr>
<tr>
<td>Patrick José Pereira <!-- 1 --></td>
<td>Paul Brown <!-- 1 --></td>
</tr>
<tr>
<td>Paul Gideon Dann <!-- 1 --></td>
<td>Paulo Barreto <!-- 1 --></td>
</tr>
<tr>
<td>Paulo Moura Guedes <!-- 1 --></td>
<td>Pavel Pertsev <!-- 1 --></td>
</tr>
<tr>
<td>Pedro Gimeno <!-- 1 --></td>
<td>Per Winkvist <!-- 1 --></td>
</tr>
<tr>
<td>Peter J. Mello <!-- 1 --></td>
<td>Peter Mello <!-- 1 --></td>
</tr>
<tr>
<td>Peter Penz <!-- 1 --></td>
<td>Philip K. Gisslow <!-- 1 --></td>
</tr>
<tr>
<td>Philippe Fremy <!-- 1 --></td>
<td>Phu Hung Nguyen <!-- 1 --></td>
</tr>
<tr>
<td>Quinten Kock <!-- 1 --></td>
<td>Rafał Miłecki <!-- 1 --></td>
</tr>
<tr>
<td>Ralf Jung <!-- 1 --></td>
<td>Ralf Nolden <!-- 1 --></td>
</tr>
<tr>
<td>Ramon Zarazua <!-- 1 --></td>
<td>Randy Kron <!-- 1 --></td>
</tr>
<tr>
<td>Raphael Rosch <!-- 1 --></td>
<td>Richard Mader <!-- 1 --></td>
</tr>
<tr>
<td>Robert Gruber <!-- 1 --></td>
<td>Robert-André Mauchin <!-- 1 --></td>
</tr>
<tr>
<td>Rohan Garg <!-- 1 --></td>
<td>Rolf Magnus <!-- 1 --></td>
</tr>
<tr>
<td>Roman Gilg <!-- 1 --></td>
<td>Roman Hujer <!-- 1 --></td>
</tr>
<tr>
<td>Ruediger Gad <!-- 1 --></td>
<td>Sam Greenwood <!-- 1 --></td>
</tr>
<tr>
<td>Samu Voutilainen <!-- 1 --></td>
<td>Sandro Giessl <!-- 1 --></td>
</tr>
<tr>
<td>Sascha Cunz <!-- 1 --></td>
<td>Sean Gillespie <!-- 1 --></td>
</tr>
<tr>
<td>Shalok Shalom <!-- 1 --></td>
<td>Shane Wright <!-- 1 --></td>
</tr>
<tr>
<td>Simone Scalabrino <!-- 1 --></td>
<td>Stefan Gehn <!-- 1 --></td>
</tr>
<tr>
<td>Steve Mokris <!-- 1 --></td>
<td>Sven Greb <!-- 1 --></td>
</tr>
<tr>
<td>Sven Lüppken <!-- 1 --></td>
<td>Thomas Capricelli <!-- 1 --></td>
</tr>
<tr>
<td>Thomas Horstmeyer <!-- 1 --></td>
<td>Thomas Jarosch <!-- 1 --></td>
</tr>
<tr>
<td>Thomas McGuire <!-- 1 --></td>
<td>Thomas Reitelbach <!-- 1 --></td>
</tr>
<tr>
<td>Till Schfer <!-- 1 --></td>
<td>Tim Jansen <!-- 1 --></td>
</tr>
<tr>
<td>Tobias Kündig <!-- 1 --></td>
<td>Tom Albers <!-- 1 --></td>
</tr>
<tr>
<td>Tore Melangen Havn <!-- 1 --></td>
<td>Vladimír Vondruš <!-- 1 --></td>
</tr>
<tr>
<td>Wagner M Cunha <!-- 1 --></td>
<td>Wale Afolabi <!-- 1 --></td>
</tr>
<tr>
<td>Waqar Ahmed        <!-- 1 --></td>
<td>Werner Trobin <!-- 1 --></td>
</tr>
<tr>
<td>Wilco Greven <!-- 1 --></td>
<td>Will Stephenson <!-- 1 --></td>
</tr>
<tr>
<td>Wojciech Stachurski <!-- 1 --></td>
<td>Wouter Becq <!-- 1 --></td>
</tr>
<tr>
<td>Yuen Hoe Lim <!-- 1 --></td>
<td>Yuhang Zhao <!-- 1 --></td>
</tr>
<tr>
<td>Yunhe Guo <!-- 1 --></td>
<td>Zoe Clifford <!-- 1 --></td>
</tr>
<tr>
<td>aa bb <!-- 1 --></td>
<td>bombless <!-- 1 --></td>
</tr>
<tr>
<td>est 31 <!-- 1 --></td>
<td>gamazeps <!-- 1 --></td>
</tr>
<tr>
<td>hololeap hololeap <!-- 1 --></td>
<td>m.eik michalke <!-- 1 --></td>
</tr>
<tr>
<td>mdinger <!-- 1 --></td>
<td>oldherl oh <!-- 1 --></td>
</tr>
<tr>
<td>shenlebantongying <!-- 1 --></td>
<td>tfry <!-- 1 --></td>
</tr>
<tr>
<td>visualfc <!-- 1 --></td>
<td>Émeric Dupont <!-- 1 --></td>
</tr>
<tr>
<td>Сковорода Никита Андреевич <!-- 1 --></td>
</table>

